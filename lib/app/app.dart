import 'package:ecommerce_bloc_demo/repository/auth/auth_repository.dart';
import 'package:ecommerce_bloc_demo/repository/category/category_repository.dart';
import 'package:ecommerce_bloc_demo/repository/checkout/checkout_repository.dart';
import 'package:ecommerce_bloc_demo/repository/product/product_repository.dart';
import 'package:ecommerce_bloc_demo/routes/app_routes.dart';
import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/utils/app_constants.dart';
import 'package:ecommerce_bloc_demo/views/cart/bloc/cart_bloc.dart';
import 'package:ecommerce_bloc_demo/views/home/bloc/category/category_bloc.dart';
import 'package:ecommerce_bloc_demo/views/home/bloc/product/product_bloc.dart';
import 'package:ecommerce_bloc_demo/views/home/home_view.dart';
import 'package:ecommerce_bloc_demo/views/login/bloc/login_bloc.dart';
import 'package:ecommerce_bloc_demo/views/login/login_view.dart';
import 'package:ecommerce_bloc_demo/views/payment/bloc/checkout_bloc.dart';
import 'package:ecommerce_bloc_demo/views/payment_selection/bloc/payment_bloc.dart';
import 'package:ecommerce_bloc_demo/views/screens.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_bloc.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_event.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => AuthRepository(),
      child: MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => WishlistBloc()..add(StartWishlist())),
          BlocProvider(create: (_) => CartBloc()..add(StartCart())),
          BlocProvider(
            create: (context) => AuthBloc(
              authRepository: RepositoryProvider.of<AuthRepository>(context),
            ),
          ),
          BlocProvider(
            create: (context) => CheckoutBloc(
              cartBloc: context.read<CartBloc>(),
              // paymentBloc: context.read<PaymentBloc>(),
              checkoutRepository: CheckoutRepository(),
            ),
          ),
          BlocProvider(create: (_) => PaymentBloc()..add(LoadPaymentMethod())),
          BlocProvider(
              create: (_) =>
                  CategoryBloc(categoryRepository: CategoryRepository())
                    ..add(LoadCategories())),
          BlocProvider(
              create: (_) => ProductBloc(productRepository: ProductRepository())
                ..add(LoadProducts()))
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            fontFamily: "Helvetica Neue",
            primarySwatch: ColorConstants.appColor,
          ),
          builder: (BuildContext context, Widget? child) {
            final MediaQueryData data = MediaQuery.of(context);
            return MediaQuery(
                data: data.copyWith(textScaleFactor: 1), child: child!);
          },
          onGenerateRoute: AppRouter.onGenerateRoute,
          // initialRoute: LoginView.routeName,
          title: AppConstants.appName,
          home: StreamBuilder<User?>(
              stream: FirebaseAuth.instance.authStateChanges(),
              builder: (context, snapshot) {
                // If the snapshot has user data, then they're already signed in. So Navigating to the Dashboard.
                if (snapshot.hasData) {
                  return HomeView();
                }
                // Otherwise, they're not signed in. Show the sign in page.
                return const LoginView();
              }),
          // HomeView(),
        ),
      ),
    );
  }
}
