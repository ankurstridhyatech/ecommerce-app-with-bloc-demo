import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:flutter/material.dart';

class AppHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: MyClipper(),
      child: Container(
        width: double.infinity,
        height: 450.0,
        color: ColorConstants.appColor,
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

  /*  // path.lineTo(0, size.height - 150);
    path.lineTo(0,size.width / 1.8);
    path.quadraticBezierTo(
        size.width / 3, size.height , size.width , size.height / 2);
    path.lineTo(size.width , 0);
   // path.lineTo(0,size.width,);*/



    path.lineTo(0, size.height - 150);
    path.quadraticBezierTo(size.width / 2, size.height, size.width, size.height - 150);
    path.lineTo(size.width, 0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
