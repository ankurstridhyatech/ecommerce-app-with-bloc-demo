// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:get/get.dart';
// import 'package:stationery_app/routes/app_pages.dart';
// import 'package:stationery_app/style/color_constants.dart';
// import 'package:stationery_app/style/dimensions.dart';
// import 'package:stationery_app/utils/app_constants.dart';
// import 'package:stationery_app/utils/image_paths.dart';
// import 'package:stationery_app/utils/ui_utils.dart';
// import 'package:stationery_app/views/drawer/drawer_controller.dart';
// import 'package:stationery_app/views/welcome/welcome_controller.dart';
// import 'package:stationery_app/widgets/rounded_button.dart';
// import 'package:stationery_app/widgets/text_form_field_red_box.dart';
//
//
// class DrawerView extends StatelessWidget {
//   DrawerView({Key? key}) : super(key: key);
//
//   CustomDrawerController drawerController = Get.put(CustomDrawerController());
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Drawer(
//           backgroundColor: ColorConstants.white,
//           child: SingleChildScrollView(
//             child: Container(
//               //  height: Dimensions.screenHeight,
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Column(
//                     //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Padding(
//                         padding: const EdgeInsets.only(
//                           bottom: 30.0,
//                           left: 20.0,
//                           right: 20.0,
//                         ),
//                         child: Container(
//                           //  color: Colors.green,
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                               Padding(
//                                 padding: EdgeInsets.only(
//                                     top: Dimensions.screenHeight * 0.035),
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.center,
//                                   children: [
//                                     Container(
//                                       height: 80,
//                                       width: 80,
//                                     //  color: Colors.green,
//                                       child: Image(
//                                           // height: 80,
//                                           // width: 80,
//                                           color: ColorConstants.appColor,
//                                           image: AssetImage(ImagePath.bookLogo)),
//                                     ),
//                                     Padding(
//                                       padding: const EdgeInsets.only(
//                                           top: 0.0, bottom: 10),
//                                       child: Center(
//                                         child: Text(
//                                           "BookBeginning",
//                                           style: TextStyle(
//                                               fontSize: Dimensions.fontSize22,
//                                               color: ColorConstants.black,
//                                               fontWeight: FontWeight.bold),
//                                         ),
//                                       ),
//                                     ),
//                                     Container(
//                                       color: ColorConstants.appColor,
//                                       height: 0.5,
//                                       width: Dimensions.screenWidth / 4,
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               SizedBox(
//                                 height: Dimensions.screenWidth / 22,
//                               ),
//                               Padding(
//                                 padding: const EdgeInsets.only(bottom: 0.0),
//                                 child: Text(
//                                   "Login",
//                                   textAlign: TextAlign.start,
//                                   style: TextStyle(
//                                       fontSize: 30,
//                                       fontWeight: FontWeight.bold,
//                                       color: ColorConstants.black),
//                                 ),
//                               ),
//
//
//                             ],
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         );
//   }
// }
