// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:ecommerce_bloc_demo/generated/i18n.dart';
// import 'package:ecommerce_bloc_demo/routes/app_pages.dart';
// import 'package:ecommerce_bloc_demo/utils/app_constants.dart';
// import 'package:ecommerce_bloc_demo/widgets/rounded_border_button.dart';
// import 'package:ecommerce_bloc_demo/widgets/rounded_button.dart';
//
// import '../../style/color_constants.dart';
// import '../../style/dimensions.dart';
// import '../../utils/image_paths.dart';
//
// class LoginView extends StatelessWidget {
//   LoginView({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     Dimensions.screenWidth = MediaQuery.of(context).size.width;
//     Dimensions.screenHeight = MediaQuery.of(context).size.height;
//     return GetBuilder(
//       init: SplashController(),
//       builder: (SplashController controller) {
//         return Scaffold(
//           body: Container(
//             color: ColorConstants.white,
//             height: Dimensions.screenHeight,
//             width: Dimensions.screenWidth,
//             child: Column(
//
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             //  mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//
//                 Padding(
//                   padding: const EdgeInsets.all(20.0),
//                   child: Column(
//
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       SizedBox(
//                         height: Dimensions.screenWidth / 5,
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.only(left: 8.0),
//                         child: Text(
//                           "Stationery App",
//                           style: TextStyle(
//                               fontSize: Dimensions.fontSize26,
//                               fontWeight: FontWeight.bold),
//                         ),
//                       ),
//                       SizedBox(
//                         height: Dimensions.screenWidth / 15,
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.only(left: 8.0),
//                         child: Text(
//                           "Lorem ipsum is simply dummy text of the printing and typesetting industry make a type specimen book",
//                           style: TextStyle(
//                               color: ColorConstants.grayA5,
//                               fontSize: Dimensions.fontSize18,
//                               fontWeight: FontWeight.w400),
//                         ),
//                       ),
//
//                     ],
//                   ),
//                 ),
//
//                 Padding(
//                   padding: const EdgeInsets.only(bottom: 30.0),
//                   child: Container(
//                   //  color: Colors.green,
//                     child: Column(
//                       children: [
//                         SizedBox(
//                           height: Dimensions.screenWidth / 8,
//                         ),
//
//                         Padding(
//                           padding:  EdgeInsets.only(right: 20,left: 20),
//                           child: RoundedBorderButton(
//                               onTap: () {
//                                 Get.offNamedUntil(
//                                     Routes.HOME, (route) => false);
//                                 // //  Get.back();
//                               },
//                               textcolor: ColorConstants.black,
//                               color: Colors.transparent,
//                               borderColor: ColorConstants.black,
//                               buttonName: 'Login'),
//                         ),
//                         SizedBox(
//                           height: Dimensions.screenWidth / 10,
//                         ),
//                         Padding(
//                           padding:  EdgeInsets.only(right: 20,left: 20),
//                           child: RoundedButton(
//                               onTap: () {
//                                 Get.offNamedUntil(
//                                     Routes.HOME, (route) => false);
//                                 // //  Get.back();
//                               },
//                               textcolor: ColorConstants.white,
//                               color: ColorConstants.appColor,
//                               buttonName: 'Create an Account'),
//                         ),
//                         SizedBox(
//                           height: Dimensions.screenWidth / 10,
//                         ),
//
//                         Padding(
//                           padding: const EdgeInsets.only(bottom: 12.0),
//                           child: Text(
//                             "Skip",
//                             style: TextStyle(
//                                 color: ColorConstants.black,
//                                 fontSize: Dimensions.fontSize18,
//                                 fontWeight: FontWeight.w400),
//                           ),
//                         ),
//                         Container(
//                          color: ColorConstants.appColor,
//                           height: 1.5,
//                           width: Dimensions.screenWidth / 7,
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         );
//       },
//     );
//   }
// }
