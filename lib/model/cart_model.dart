import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:equatable/equatable.dart';

class Cart extends Equatable {
  final List<CategoryProduct> products;

  const Cart({this.products = const <CategoryProduct>[]});

  @override
  List<Object?> get props => [products];

  Map? productQuantity(products) {
    var quantity = {};

    products.forEach((product) {
      if (!quantity.containsKey(product)) {
        quantity[product] = 1;
      } else {
        quantity[product] += 1;
      }
    });
    return quantity;
  }

  double get subtotal =>
      products.fold(0, (total, current) => total + current.price!.toInt());

  double deliveryFee(subtotal) {
    if (subtotal >= 900.0) {
      return 0.0;
    } else if (subtotal == 0) {
      return 0.0;
    } else {
      return 10.0;
    }
  }

  double total(subtotal, deliveryFee) {
    return subtotal + deliveryFee(subtotal);
  }

  String freeDelivery(subtotal) {
    if (subtotal >= 900.0) {
      return "You have Free Delivery";
    } else {
      double missing = 900.0 - subtotal;
      return "Add \u{20B9}${missing.toStringAsFixed(2)} for Free Delivery";
    }
  }

  String get subtotalString => subtotal.toString();

  String get totalString => total(subtotal, deliveryFee).toStringAsFixed(2);

  String get deliveryFeeString => deliveryFee(subtotal).toStringAsFixed(2);

  String get freeDeliveryString => freeDelivery(subtotal);

// static  List<Cart> products = [
//   Cart(
//     id: 1,
//     title: 'Notebooks',
//     price: 236,
//     imageUrl:
//     // 'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
//     'https://tiimg.tistatic.com/fp/1/006/392/eco-friendly-school-note-book-243-w410.jpg',
//   ),
//   Cart(
//       id: 2,
//       title: 'School Books',
//       price: 204,
//       // imageUrl: 'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Men-Legend-BlackMatte-3.4_672x672.jpg?v=1600886623'),
//       imageUrl:
//       'https://images-na.ssl-images-amazon.com/images/I/81yYeKc+U7L.jpg'),
//   Cart(
//       id: 3,
//       title: 'Office Supplies',
//       price: 196,
//       imageUrl:
//       // 'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Men-Cavalier-Toffee-210402-3.jpg?v=1618424894'),
//       'https://thumbs.dreamstime.com/z/stationery-set-22104838.jpg'),
//   Cart(
//       id: 4,
//       title: 'Calender',
//       price: 20,
//       imageUrl:
//       // 'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Men-PremierLowTop-Black-3.4.jpg?v=1600270679'),
//       'https://thumbs.dreamstime.com/z/calender-4737855.jpg'),
// ];
}
