import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class Category extends Equatable {
  final String? title;
  final String? imageUrl;

  const Category({
    this.title,
    this.imageUrl,
  });

  @override
  List<Object?> get props => [
        title,
        imageUrl,
      ];

  static Category fromSnapshot(DocumentSnapshot snapshot) {
    Category category = Category(
      title: snapshot['name'],
      imageUrl: snapshot['imgUrl'],
    );
    return category;
  }

  static List<Category> categories = [
    const Category(
      title: 'School Book Store',
      imageUrl:
          'https://www.pngkey.com/png/full/443-4432695_school-library-online-book-store.png',
    ),
    const Category(
        title: 'School Stationery',
        imageUrl:
            'https://w7.pngwing.com/pngs/1007/931/png-transparent-paper-office-supplies-stationery-business-material-retail-people-office-thumbnail.png'),
  ];
}
