import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:equatable/equatable.dart';

class Checkout extends Equatable {
  final String? fullName;
  final String? email;
  final String? address;
  final String? city;
  final String? country;
  final String? zipCode;
  final List<CategoryProduct>? products;
  final String? subTotal;
  final String? deliveryFee;
  final String? total;


  const Checkout({
    required this.fullName,
      required this.email,
      required this.address,
      required this.city,
      required this.country,
      required this.zipCode,
      required this.subTotal,
      required this.deliveryFee,
      required this.products,
      required this.total});

  @override
  // TODO: implement props
  List<Object> get props => [
    fullName!,
    email!,
    address!,
    city!,
    country!,
    zipCode!,
    subTotal!,
    deliveryFee!,
    products!,
    total!
  ];

  Map<String, Object> toDocument (){
    Map customerAddress = Map();
    customerAddress['address'] = address;
    customerAddress['city'] = city;
    customerAddress['zipCode'] = zipCode;
    customerAddress['country'] = country;

    return {
      'customerAddress': customerAddress,
      'customerEmail': email!,
      'customerName': fullName!,
      'products': products!.map((product) => product.title).toList(),
      'subTotal': subTotal!,
      'total': total!,
      'deliveryFee': deliveryFee!,
    };
  }
}
