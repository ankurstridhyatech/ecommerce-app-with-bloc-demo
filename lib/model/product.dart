import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class CategoryProduct extends Equatable {
  final int? id;
  final int? price;
  final String? title;
  final String? imageUrl;
  bool? isFavourite;

  CategoryProduct(
      {this.id,
      this.title,
      this.imageUrl,
      this.price,
      this.isFavourite = false});

  @override
  List<Object?> get props => [id, title, price, imageUrl, isFavourite];

  static CategoryProduct fromSnapshot (DocumentSnapshot snapshot){
    CategoryProduct categoryProduct = CategoryProduct(
      id: snapshot['id'],
      title: snapshot['title'],
      price: snapshot['price'],
      isFavourite: snapshot['isFavourite'],
      imageUrl: snapshot['imageUrl'],
    );
    return categoryProduct;
  }

  static List<CategoryProduct> products = [
    CategoryProduct(
      id: 1,
      title: 'Notebooks',
      price: 236,
      imageUrl:
          'https://tiimg.tistatic.com/fp/1/006/392/eco-friendly-school-note-book-243-w410.jpg',
    ),
    CategoryProduct(
        id: 2,
        title: 'School Books',
        price: 204,
        imageUrl:
            'https://images-na.ssl-images-amazon.com/images/I/81yYeKc+U7L.jpg'),
    CategoryProduct(
        id: 3,
        title: 'Office Supplies',
        price: 196,
        imageUrl:
            'https://thumbs.dreamstime.com/z/stationery-set-22104838.jpg'),
    CategoryProduct(
        id: 4,
        title: 'Calender',
        price: 20,
        imageUrl: 'https://thumbs.dreamstime.com/z/calender-4737855.jpg'),
  ];
}
