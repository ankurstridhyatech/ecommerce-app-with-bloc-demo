import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:equatable/equatable.dart';

class Wishlist extends Equatable {
  final List<CategoryProduct>? products;

  const Wishlist({
    this.products = const <CategoryProduct>[],
  });

  @override
  // TODO: implement props
  List<Object> get props => [products!];
}
