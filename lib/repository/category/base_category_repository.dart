import 'package:ecommerce_bloc_demo/model/models.dart';


abstract class BaseCategoryRepository {
  Stream<List<Category>> getAllCategories();
}