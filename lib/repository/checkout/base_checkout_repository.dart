import 'package:ecommerce_bloc_demo/model/models.dart';


abstract class BaseCheckoutRepository {
  Future<void> addCheckout(Checkout checkout);
}