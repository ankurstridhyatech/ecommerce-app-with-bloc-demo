import 'package:ecommerce_bloc_demo/model/models.dart';


abstract class BaseProductRepository {
  Stream<List<CategoryProduct>> getAllProducts();
}