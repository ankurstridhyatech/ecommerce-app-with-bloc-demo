import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecommerce_bloc_demo/model/models.dart';
import 'package:ecommerce_bloc_demo/repository/product/base_product_repository.dart';

class ProductRepository extends BaseProductRepository {
  final FirebaseFirestore? _firebaseFirestore;

  ProductRepository({FirebaseFirestore? firebaseFirestore})
      : _firebaseFirestore = firebaseFirestore ?? FirebaseFirestore.instance;

  @override
  Stream<List<CategoryProduct>> getAllProducts() {
    return _firebaseFirestore!.collection('products').snapshots().map(
        (snapshot) =>
            snapshot.docs.map((doc) => CategoryProduct.fromSnapshot(doc)).toList());
  }
}
