import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:ecommerce_bloc_demo/utils/utility.dart';
import 'package:ecommerce_bloc_demo/views/cart/cart_view.dart';
import 'package:ecommerce_bloc_demo/views/home/home_view.dart';
import 'package:ecommerce_bloc_demo/views/login/login_view.dart';
import 'package:ecommerce_bloc_demo/views/sign_up/signup_view.dart';
import 'package:ecommerce_bloc_demo/views/order_confirmation/order_confirmation_view.dart';
import 'package:ecommerce_bloc_demo/views/payment_selection/payment_selection_view.dart';
import 'package:ecommerce_bloc_demo/views/product_details/product_details_view.dart';
import 'package:ecommerce_bloc_demo/views/profile/profile_view.dart';
import 'package:ecommerce_bloc_demo/views/splash/splash_view.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/wishlist_view.dart';
import 'package:ecommerce_bloc_demo/views/payment/payment_view.dart';
import 'package:flutter/material.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    printf("This is route: ${settings.name} ");

    switch (settings.name) {
      case '/':
        return HomeView.route();
      case HomeView.routeName:
        return HomeView.route();
      case SplashView.routeName:
        return SplashView.route();
      case LoginView.routeName:
        return LoginView.route();
        case SignUpView.routeName:
        return SignUpView.route();
      case CartView.routeName:
        return CartView.route();
      case ProfileView.routeName:
        return ProfileView.route();
      case WishlistView.routeName:
        return WishlistView.route();
      case ProductDetailsView.routeName:
        return ProductDetailsView.route(
            product: settings.arguments as CategoryProduct);
      case PaymentView.routeName:
        return PaymentView.route();
      case PaymentSelectionView.routeName:
        return PaymentSelectionView.route();
        case OrderConfirmationView.routeName:
        return OrderConfirmationView.route();

      default:
        return _errorRoute();
    }
  }

  static Route _errorRoute() {
    return MaterialPageRoute(
        settings: const RouteSettings(name: '/error'),
        builder: (_) => Scaffold(
                appBar: AppBar(
              title: const Text("Error"),
            )));
  }
}
