import 'package:flutter/material.dart';

class ColorConstants {
  static const white = Colors.white;
  static const black = Colors.black;
  static const blue = Color(0xFF00A9CC);
  static const yellow = Color(0xFFF4EB14);
  static const yellow31 = Color(0xFFfdcc31);
  static const blue12 = Color(0xFF12B2D5);
  static const greendb = Color(0xFFbfecdb);
  static const gray7A = Color(0xFF72777A); //#F7F9FA
  static const grayE5 = Color(0xFFE3E5E5);
  static const grayE6 = Color(0xFFE6E6E6);
  static const grayF8 = Color(0xFFF4F6F8);
  static const grayA5 = Color(0xFFA8A5A5);
  static const grayB5 = Color(0xFFB5B5B5);
  static const gray84 = Color(0xFF848484);
  static const gray72 = Color(0xFF727272);
  static const grayF5 = Color(0xFFF5F5F5);
  static const gray949599 = Color(0xFF949599);
  static const grayDB = Color(0xFFDBDBDB);
  static const grayf4 = Color(0xFFf4f4f4);
  static const grayfb = Color(0xFFf7f6fb);
  static const gray70 = Color(0xFF707070);
  static const gray46 = Color(0xFF464848);
  static const gray62 = Color(0xFF626161);
  static const gray1C = Color(0xFF1C1B1B);
  static const black53 = Color(0xFF535353);
  static const black44 = Color(0xFF444444);
  static const black1617 = Color(0xFF161717);
  static const black02 = Color(0xFF020202);
  static const primary = Color(0xFF006766);
  static const grey = Color(0xFFE5E5E5);
  static const transparent = Color(0x00000000);
  static const brown = Color(0xFF8D634A);
  static const green = Color(0xFF13B238);
  static const red = Color(0xFFBC0202);
  static const green13 = Color(0xFF13B238);
  static const green00 = Color(0xFF00AA3B);
  static const green2D = Color(0xFF53BD2D);
  static const redB6 = Color(0xFFB60012);
  static const blue11 = Color(0xFF119ED1);
  static const blue13 = Color(0xFF13B0D4);
  static const eventaquide = Color(0xFF12B2D5);
  static const grayF7 = Color(0xFFEDF0F7);
  static const grayD7 = Color(0xFFD7D6D7);
  static const gray8B = Color(0xFF8D898B);
  static const greenB2 = Color(0xFF50DFB2);
  static const greenBB = Color(0xFF95E7BB);
  static const green33 = Color(0xFF224C33);
  static MaterialColor appColor = const MaterialColor(
    0xFF00D3B9,
    <int, Color>{
      50: Color(0xFF00D3B9),
      100: Color(0xFF00D3B9),
      200: Color(0xFF00D3B9),
      300: Color(0xFF00D3B9),
      400: Color(0xFF00D3B9),
      500: Color(0xFF00D3B9),
      600: Color(0xFF00D3B9),
      700: Color(0xFF00D3B9),
      800: Color(0xFF00D3B9),
      900: Color(0xFF00D3B9),
    },
  );
}
