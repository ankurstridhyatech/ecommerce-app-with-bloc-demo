import 'package:flutter/material.dart';

import 'color_constants.dart';
import 'dimensions.dart';

const textBold = TextStyle(
  color: ColorConstants.black,
  fontWeight: FontWeight.w800,
  fontSize: Dimensions.fontSizeDefault,
);

const drawerText = TextStyle(
  color: ColorConstants.black,
  fontWeight: FontWeight.w500,
  fontSize: Dimensions.fontSize18,
);

const textRegular = TextStyle(
  color: ColorConstants.black,
  fontSize: Dimensions.fontSizeDefault,
);
