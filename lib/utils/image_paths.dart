class ImagePath {
  static const String placeholderHorizontal =
      'assets/images/placeholder_horizontal.png';
  static const String placeholderSquare = 'assets/images/placeholder.png';
  static const String checkInImage = 'assets/images/ic_checked_in.svg';
  static const String checkOutImage = 'assets/images/ic_checked_out.svg';
  static const String appName = 'assets/images/ic_app_name.png';
  static const String launchSquid = 'assets/images/logo_launchsquid.png';
  static const String eventSquidLogo = 'assets/images/logo_eventsquid.png';
  static const String eventSquidLogoNoTag =
      'assets/images/logo_eventsquid_no_tag.png';

  // static const String splashLogo = 'assets/images/splash_image.png';
  static const String splashLogo = 'assets/images/stationers_logo.jpg';
  static const String splashQR = 'assets/images/splashQR.svg';
  static const String inreach = 'assets/images/logo_inreach.png';
  static const String unChecked = 'assets/images/ic_uncheck.png';
  static const String unCheckedSvg = 'assets/images/ic_uncheck.svg';
  static const String toolbarQR = 'assets/images/ic_metro_qrcode.svg';
  static const String close = 'assets/images/ic_close.png';
  static const String info = 'assets/images/ic_info.png';
  static const String checkBoxFill = 'assets/images/checkbox_empty.png';
  static const String checkBoxEmpty = 'assets/images/checkbox_fill.png';
  static const String scanLeft = 'assets/images/scan_left.png';
  static const String scanLeftBlack = 'assets/images/scan_left_black.png';
  static const String scanRight = 'assets/images/scan_right.png';
  static const String scanRightBlack = 'assets/images/scan_left_black.png';
  static const String userCheck = 'assets/images/user_check.svg';
  static const String file = 'assets/images/ic_file.svg';
  static const String qrCode = 'assets/images/ic_qr_code.svg';
  static const String stats = 'assets/images/ic_stats.svg';
  static const String event = 'assets/images/ic_event.svg';
  static const String questionMark = 'assets/images/ic_question_mark.svg';

  static const String homeDark = 'assets/images/home_dark.png';
  static const String homeLight = 'assets/images/home_light.png';
  static const String historyDark = 'assets/images/history_dark.png';
  static const String historyLight = 'assets/images/history_light.png';
  static const String couponDark = 'assets/images/coupon_dark.png';
  static const String couponLight = 'assets/images/coupon_light.png';
  static const String supportDark = 'assets/images/support_dark.png';
  static const String supportLight = 'assets/images/support_light.png';

  static const String home = 'assets/images/home_light.png';

  static const String category = 'assets/icons/category2.png';
  static const String cart = 'assets/icons/cart2.png';
  static const String profile = 'assets/icons/user1.png';
  static const String search = 'assets/icons/search.svg';
  static const String stationery = 'assets/images/stationery.png';

  // static const String bookLogo = 'assets/images/book_logo.png';
  static const String bookLogo = 'assets/images/book_logo_new.png';
  static const String rupeeLogo = 'assets/icons/rupee.png';

  static const String notification = 'assets/icons/notification.svg';
  static const String wishlist = 'assets/icons/wishlist.png';

  static const String pwVisible = 'assets/icons/ic_pw_visible.svg';
  static const String pwInvisible = 'assets/icons/ic_pw_invisible.svg';

  static const String googleIcon = 'assets/icons/ic_fb.svg';
}
