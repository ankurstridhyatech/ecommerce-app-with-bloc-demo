import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/utils/image_paths.dart';
import 'package:ecommerce_bloc_demo/views/cart/bloc/cart_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartItems extends StatefulWidget {
  const CartItems({
    Key? key,
    this.product,
    this.quantity,
  }) : super(key: key);
  final CategoryProduct? product;
  final int? quantity;

  @override
  State<CartItems> createState() => _CartItemsState();
}

class _CartItemsState extends State<CartItems> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      child: Dismissible(
          key: UniqueKey(),
          direction: DismissDirection.endToStart,
          onDismissed: (direction) {
            setState(() {
              // print(widget.productId);
              // cartController.removeItem(widget.productId);
            });
          },
          background: Container(
            color: Theme.of(context).errorColor,
            alignment: Alignment.centerRight,
            padding: const EdgeInsets.only(right: 20),
            margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 4),
            child: const Icon(
              Icons.delete,
              color: Colors.white,
              size: 40,
            ),
          ),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: ColorConstants.grayf4,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 80,
                  width: 80,
                  child: ClipRRect(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(60),
                      topRight: Radius.circular(60),
                      bottomLeft: Radius.circular(60),
                      bottomRight: Radius.circular(60),
                    ),
                    child: Image.network(
                      widget.product!.imageUrl ?? "",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: Dimensions.screenWidth * 0.33,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.product!.title ?? "",
                                style: const TextStyle(
                                    color: Colors.black, fontSize: 16),
                                maxLines: 2,
                              ),
                              const SizedBox(height: 10),
                              Row(
                                children: [
                                  SizedBox(
                                    height: 20,
                                    width: 20,
                                    child: Image.asset(
                                      ImagePath.rupeeLogo,
                                    ),
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      text: "${widget.product!.price ?? ""}",
                                      style: const TextStyle(
                                          fontWeight: FontWeight.w600,
                                          color: ColorConstants.black,
                                          fontSize: 18),
                                      children: [
                                        TextSpan(
                                            text: " x " "${widget.quantity}",
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          // color: Colors.green,
                          width: Dimensions.screenWidth * 0.02,
                        ),
                        Chip(
                          backgroundColor: ColorConstants.appColor,
                          label: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Row(
                              children: [
                                Text(
                                  '${(widget.product!.price ?? "" * 1)}',
                                  style: const TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          BlocBuilder<CartBloc, CartState>(
                              builder: (context, state) {
                            return buildOutlineButton(
                              icon: Icons.remove,
                              press: () {
                                context.read<CartBloc>().add(
                                    RemoveFromCart(product: widget.product));
                              },
                            );
                          }),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 20 / 2),
                            child: Text(
                              "${widget.quantity}",
                              style: Theme.of(context).textTheme.headline6,
                            ),
                          ),
                          BlocBuilder<CartBloc, CartState>(
                              builder: (context, state) {
                            return buildOutlineButton(
                              icon: Icons.add,
                              press: () {
                                context
                                    .read<CartBloc>()
                                    .add(AddToCart(product: widget.product));
                              },
                            );
                          }),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )),
    );
  }

  SizedBox buildOutlineButton({IconData? icon, VoidCallback? press}) {
    return SizedBox(
      width: 30,
      height: 30,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          padding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        onPressed: press,
        child: Icon(
          icon,
          size: 18,
        ),
      ),
    );
  }
}
