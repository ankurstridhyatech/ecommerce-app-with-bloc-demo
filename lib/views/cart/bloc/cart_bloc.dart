import 'package:ecommerce_bloc_demo/model/models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'cart_event.dart';

part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc() : super(CartLoadingState()) {
    on<StartCart>((event, emit) async {
      await Future<void>.delayed(const Duration(seconds: 1));
      emit(CartLoadingState());
      try {
        emit(CartLoadedState());
      } catch (e) {
        emit(CartErrorState(
          message: e.toString(),
        ));
      }
    });

    on<AddToCart>((AddToCart event, emit) async {
      final state = this.state; // local variable

      emit(CartLoadingState());

      if (state is CartLoadedState) {
        try {
          emit(CartLoadedState(
              cart: Cart(
                  products: List.from((state).cart!.products)
                    ..add(event.product!))));
          // if (state.cart != null) {
          //   emit(CartLoadedState(
          //       cart: Cart(
          //           products: List.from((state).cart!.products)
          //             ..add(event.product!))));
          // } else {
          //   List<CategoryProduct> list = [];
          //   list.add(event.product!);
          //   var cart = Cart(products: list);
          //   emit(CartLoadedState(cart: cart));
          // }
        } catch (e) {
          emit(CartErrorState(
            message: e.toString(),
          ));
        }
      }
    });

    on<RemoveFromCart>((RemoveFromCart event, emit) async {
      final state = this.state; // local variable
      emit(CartLoadingState());

      if (state is CartLoadedState) {
        try {
          emit(CartLoadedState(
              cart: Cart(
                  products: List.from(state.cart!.products)
                    ..remove(event.product!))));
        } catch (e) {
          emit(CartErrorState(
            message: e.toString(),
          ));
        }
      }
    });
  }

  Stream<CartState> mapEventToState(CartEvent event) async* {
    if (event is StartCart) {
      yield* _mapStartCartToState();
    } else if (event is AddToCart) {
      yield* _mapAddCartToState(event, state);
    } else if (event is RemoveFromCart) {
      yield* _mapRemoveCartToState(event, state);
    }
  }

  Stream<CartState> _mapStartCartToState() async* {
    yield CartLoadingState();
    try {
      await Future<void>.delayed(const Duration(seconds: 1));
      yield CartLoadedState();
    } catch (_) {}
  }

  Stream<CartState> _mapAddCartToState(
    AddToCart event,
    CartState state,
  ) async* {
    if (state is CartLoadedState) {
      try {
        yield CartLoadedState(
            cart: Cart(
                products: List.from(state.cart!.products)
                  ..add(event.product!)));
      } catch (_) {}
    }
  }

  Stream<CartState> _mapRemoveCartToState(
    RemoveFromCart event,
    CartState state,
  ) async* {
    if (state is CartLoadedState) {
      try {
        yield CartLoadedState(
            cart: Cart(
                products: List.from(state.cart!.products)
                  ..remove(event.product!)));
      } catch (_) {}
    }
  }
}
