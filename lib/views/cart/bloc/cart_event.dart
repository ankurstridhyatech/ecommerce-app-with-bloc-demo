// import 'package:ecommerce_bloc_demo/model/product.dart';
// import 'package:equatable/equatable.dart';
// import 'package:ecommerce_bloc_demo/model/models.dart';

part of 'cart_bloc.dart';

// import 'package:ecommerce_bloc_demo/model/product.dart';

abstract class CartEvent extends Equatable {
  const CartEvent();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class StartCart extends CartEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AddToCart extends CartEvent {
  final CategoryProduct? product;

  const AddToCart({this.product});

  @override
  // TODO: implement props
  List<Object> get props => [product!];
}

class RemoveFromCart extends CartEvent {
  final CategoryProduct? product;

  const RemoveFromCart({this.product});

  @override
  // TODO: implement props
  List<Object> get props => [product!];
}
