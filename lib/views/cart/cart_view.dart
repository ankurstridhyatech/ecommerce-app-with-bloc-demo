import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/views/cart/Components/cart_items.dart';
import 'package:ecommerce_bloc_demo/views/cart/bloc/cart_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartView extends StatelessWidget {
  const CartView({Key? key}) : super(key: key);

  static const String routeName = '/cart';

  static Route route() {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (_) => const CartView());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorConstants.white,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: ColorConstants.appColor,
          title: Column(
            children: [
              const Text(
                "Your Cart",
                style: TextStyle(color: Colors.white),
              ),
              BlocBuilder<CartBloc, CartState>(builder: (context, state) {
                if (state is CartLoadedState) {
                  return state.cart!
                          .productQuantity(state.cart!.products)!
                          .keys
                          .isNotEmpty
                      ? Text(
                          " Items"
                          " ${state.cart!.productQuantity(state.cart!.products)!.keys.length}",
                          style: Theme.of(context)
                              .textTheme
                              .caption!
                              .copyWith(color: ColorConstants.white),
                        )
                      : Text(
                          "No Items",
                          style: Theme.of(context)
                              .textTheme
                              .caption!
                              .copyWith(color: ColorConstants.white),
                        );
                }
                return Container();
              }),
            ],
          ),
        ),
        body: BlocBuilder<CartBloc, CartState>(builder: (context, state) {
          if (state is CartLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is CartLoadedState) {
            return state.cart!
                    .productQuantity(state.cart!.products)!
                    .keys
                    .isNotEmpty
                ? SingleChildScrollView(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 20.0, left: 20, right: 20),
                            child: state.cart!
                                        .productQuantity(state.cart!.products)!
                                        .keys
                                        .length ==
                                    1
                                ? Text(
                                    " ${state.cart!.productQuantity(state.cart!.products)!.keys.length}"
                                    " Item in Cart",
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17),
                                    maxLines: 2,
                                  )
                                : Text(
                                    " ${state.cart!.productQuantity(state.cart!.products)!.keys.length}"
                                    " Items in Cart",
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17),
                                    maxLines: 2,
                                  ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 12),
                            child: state.cart != null
                                ? ListView.builder(
                                    shrinkWrap: true,
                                    physics: const ScrollPhysics(),
                                    itemCount: state.cart!
                                        .productQuantity(state.cart!.products)!
                                        .keys
                                        .length,
                                    itemBuilder: (context, index) => CartItems(
                                      product: state.cart!
                                          .productQuantity(
                                              state.cart!.products)!
                                          .keys
                                          .elementAt(index),
                                      quantity: state.cart!
                                          .productQuantity(
                                              state.cart!.products)!
                                          .values
                                          .elementAt(index),
                                    ),
                                  )
                                : Container(),
                          ),
                          const Padding(
                            padding:
                                EdgeInsets.only(top: 20.0, left: 20, right: 20),
                            child: Text(
                              "Payment Details",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17),
                              maxLines: 2,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 20.0, left: 20, right: 20),
                            child: Container(
                              //color: Colors.deepOrange,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 16),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ColorConstants.grayf4,
                              ),
                              child: Column(
                                //  crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text(
                                        "Total items",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400),
                                      ),
                                      Text(
                                        " ${state.cart!.productQuantity(state.cart!.products)!.keys.length}",
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 18),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        const Text(
                                          "SubTotal",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400),
                                        ),
                                        Text(
                                          "\u{20B9} ${state.cart!.subtotalString}",
                                          style: const TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 18),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        const Text(
                                          "Delivery Fee",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400),
                                        ),
                                        Text(
                                          "\u{20B9} ${state.cart!.deliveryFeeString}",
                                          style: const TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 18),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: Container(
                                      // padding: EdgeInsets.all(8),
                                      height: 1,
                                      color: ColorConstants.grayB5,
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text(
                                        "Total amount",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        "\u{20B9} ${state.cart!.totalString}",
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: Dimensions.screenHeight * 0.3,
                          )
                        ]),
                  )
                : const Center(
                    child: Text(
                    "Add items in Cart",
                    style: TextStyle(fontSize: 18),
                  ));
          }
          if (state is CartErrorState) {
            return buildError(state.message);
          }
          return Container();
        }),
        bottomSheet: Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          child: TextButton(
            style: TextButton.styleFrom(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(60)),
              primary: Colors.white,
              backgroundColor: ColorConstants.appColor,
            ),
            onPressed: () {
              Navigator.pushNamed(context, "/payment");
            },
            child: const Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0),
              child: Text(
                "Check Out",
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ));
  }

  Widget buildLoading() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildError(String message) {
    return Center(
      child: Text(message),
    );
  }
}
