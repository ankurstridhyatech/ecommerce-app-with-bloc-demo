import 'dart:async';

import 'package:ecommerce_bloc_demo/model/models.dart';
import 'package:ecommerce_bloc_demo/repository/category/category_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'category_event.dart';

part 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  final CategoryRepository _categoryRepository;
  StreamSubscription? _categorySubscription;

  CategoryBloc({required CategoryRepository categoryRepository})
      : _categoryRepository = categoryRepository,
        super(CategoryLoadingState()) {
    on<LoadCategories>((event, emit) async {
      await Future<void>.delayed(const Duration(seconds: 1));
      emit(CategoryLoadingState());
      try {
        _categorySubscription?.cancel();
        _categorySubscription = _categoryRepository.getAllCategories().listen(
            (categories) => add(UpdateCategories(categories: categories)));
        emit(const CategoryLoadedState());
      } catch (e) {
        emit(CategoryErrorState(
          message: e.toString(),
        ));
      }
    });

    on<UpdateCategories>((UpdateCategories event, emit) async {
      final state = this.state; // local variable

      if (state is CategoryLoadedState) {
        try {
          emit(CategoryLoadedState(categories: event.categories!));
        } catch (e) {
          emit(CategoryErrorState(
            message: e.toString(),
          ));
        }
      }
    });
  }

  Stream<CategoryState> mapEventToState(CategoryEvent event) async* {
    if (event is LoadCategories) {
      yield* _mapLoadCategoriesToState();
    } else if (event is UpdateCategories) {
      yield* _mapUpdateCategoriesToState(event, state);
    }
  }

  Stream<CategoryState> _mapLoadCategoriesToState() async* {
    _categorySubscription?.cancel();
    _categorySubscription = _categoryRepository
        .getAllCategories()
        .listen((categories) => add(UpdateCategories(categories: categories)));
  }

  Stream<CategoryState> _mapUpdateCategoriesToState(
    UpdateCategories event,
    CategoryState state,
  ) async* {
    if (state is CategoryLoadedState) {
      try {
        yield CategoryLoadedState(categories: event.categories!);
      } catch (_) {}
    }
  }
}
