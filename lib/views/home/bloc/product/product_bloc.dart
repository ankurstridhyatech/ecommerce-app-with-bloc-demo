import 'dart:async';

import 'package:ecommerce_bloc_demo/model/models.dart';
import 'package:ecommerce_bloc_demo/repository/product/product_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'product_event.dart';

part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  final ProductRepository _productRepository;
  StreamSubscription? _productSubscription;

  ProductBloc({required ProductRepository productRepository})
      : _productRepository = productRepository,
        super(ProductLoadingState()) {
    on<LoadProducts>((event, emit) async {
      await Future<void>.delayed(const Duration(seconds: 1));
      emit(ProductLoadingState());
      try {
        _productSubscription?.cancel();
        _productSubscription = _productRepository
            .getAllProducts()
            .listen((products) => add(UpdateProducts(products: products)));
        emit(const ProductLoadedState());
      } catch (e) {
        emit(ProductErrorState(
          message: e.toString(),
        ));
      }
    });

    on<UpdateProducts>((UpdateProducts event, emit) async {
      final state = this.state; // local variable

      if (state is ProductLoadedState) {
        try {
          emit(ProductLoadedState(products: event.products!));
        } catch (e) {
          emit(ProductErrorState(
            message: e.toString(),
          ));
        }
      }
    });
  }

  Stream<ProductState> mapEventToState(ProductEvent event) async* {
    if (event is LoadProducts) {
      yield* _mapLoadProductsToState();
    } else if (event is UpdateProducts) {
      yield* _mapUpdateProductsToState(event, state);
    }
  }

  Stream<ProductState> _mapLoadProductsToState() async* {
    _productSubscription?.cancel();
    _productSubscription = _productRepository
        .getAllProducts()
        .listen((products) => add(UpdateProducts(products: products)));
  }

  Stream<ProductState> _mapUpdateProductsToState(
    UpdateProducts event,
    ProductState state,
  ) async* {
    if (state is ProductLoadedState) {
      try {
        yield ProductLoadedState(products: event.products!);
      } catch (_) {}
    }
  }
}
