import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/views/home/bloc/category/category_bloc.dart';
import 'package:ecommerce_bloc_demo/views/home/bloc/product/product_bloc.dart';
import 'package:ecommerce_bloc_demo/widgets/carousel_card.dart';
import 'package:ecommerce_bloc_demo/widgets/custom_app_bar.dart';
import 'package:ecommerce_bloc_demo/widgets/custom_bottom_bar.dart';
import 'package:ecommerce_bloc_demo/widgets/product_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeView extends StatelessWidget {
  HomeView({Key? key}) : super(key: key);

  final GlobalKey<ScaffoldState> _key = GlobalKey(); // add this

  static const String routeName = '/home';

  static Route route() {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (_) => HomeView());
  }

  @override
  Widget build(BuildContext context) {
    Dimensions.screenWidth = MediaQuery.of(context).size.width;
    Dimensions.screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: ColorConstants.white,

      appBar: const CustomAppBar(
        title: "Stationers",
      ),
      key: _key,
      // drawer: DrawerView(),
      body: SingleChildScrollView(
        child: Container(
          color: ColorConstants.white,
          child: Column(
            children: [
              BlocBuilder<CategoryBloc, CategoryState>(
                builder: (context, state) {
                  if (state is CategoryLoadingState) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  if (state is CategoryLoadedState) {
                    return CarouselSlider(

                        ///   Slider Container properties   ///

                        options: CarouselOptions(
                          height: ((1.16 * Dimensions.screenWidth) / 2),
                          enlargeCenterPage: true,
                          autoPlay: false,
                          aspectRatio: 16 / 9,
                          autoPlayCurve: Curves.fastOutSlowIn,
                          enableInfiniteScroll: true,
                          autoPlayAnimationDuration:
                              const Duration(milliseconds: 2000),
                          // viewportFraction: 0.8,
                          viewportFraction: 1,
                        ),
                        items: state.categories
                            .map((category) => CarouselCard(
                                  category: category,
                                ))
                            .toList());
                  }
                  if (state is CategoryErrorState) {
                    return buildError(state.message);
                  }
                  return Container();
                },
              ),
              // carousalSlider(),
              //
              Padding(
                padding: const EdgeInsets.only(
                    top: 20.0, left: 25, bottom: 25, right: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Top Category",
                      textScaleFactor: 1.0,
                      style:
                          TextStyle(color: ColorConstants.black, fontSize: 20),
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: const [
                          Padding(
                            padding: EdgeInsets.only(right: 4.0),
                            child: Text(
                              "View All",
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                  fontFamily: 'SofiaProBold',
                                  color: ColorConstants.black,
                                  fontSize: 14),
                            ),
                          ),
                          Icon(
                            Icons.arrow_forward_rounded,
                            size: 16,
                            color: ColorConstants.black,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    const SizedBox(
                      width: 20,
                    ),
                    BlocBuilder<ProductBloc, ProductState>(
                      builder: (context, state) {
                        if (state is ProductLoadingState) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        if (state is ProductLoadedState) {
                          return SizedBox(
                            height: Dimensions.screenHeight * 0.18,
                            // color: Colors.green,
                            child: ListView.separated(
                              shrinkWrap: true,
                              physics: const ClampingScrollPhysics(),
                              itemCount: state.products.length,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (BuildContext context, index) =>
                                  ProductCard(
                                product: state.products[index],
                              ),
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return Container(
                                  width: 20,
                                );
                              },
                            ),
                          );
                        }
                        if (state is ProductErrorState) {
                          return buildError(state.message);
                        }
                        return Container();
                      },
                    ),
                    // SizedBox(
                    //   height: Dimensions.screenHeight * 0.18,
                    //   // color: Colors.green,
                    //   child: ListView.separated(
                    //     shrinkWrap: true,
                    //     physics: const ClampingScrollPhysics(),
                    //     itemCount: CategoryProduct.products.length,
                    //     scrollDirection: Axis.horizontal,
                    //     itemBuilder: (BuildContext context, index) =>
                    //         ProductCard(
                    //       product: CategoryProduct.products[index],
                    //     ),
                    //     separatorBuilder: (BuildContext context, int index) {
                    //       return Container(
                    //         width: 20,
                    //       );
                    //     },
                    //   ),
                    // ),
                    const SizedBox(
                      width: 20,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: const CustomBottomBar(),
    );
  }

  Widget buildError(String message) {
    return Center(
      child: Text(message),
    );
  }

  Widget carousalSlider() {
    return CarouselSlider(
      items: [
        //1st Image of Slider
        Container(
          margin: const EdgeInsets.all(6.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: ColorConstants.appColor.withOpacity(0.7),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                alignment: Alignment.centerLeft,
                width: Dimensions.screenWidth,
                child: Image.network(
                    "https://www.pngkey.com/png/full/443-4432695_school-library-online-book-store.png"),
              ),
              Positioned(
                // top: (((1.16 * Dimensions.screenWidth) / 2) - 50),
                top: 0,
                left: 0.0,
                right: 0.0,
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: const [
                      Text(
                        "School Book Store",
                        textScaleFactor: 1.0,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: ColorConstants.white,
                          fontSize: 18.0,
                          fontFamily: 'SofiaProMedium',
                          // fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                // top: (((1.16 * Dimensions.screenWidth) / 2) - 50),
                top: Dimensions.screenWidth * 0.20,
                left: 0.0,
                right: 0.0,
                child: InkWell(
                  onTap: () {
                    // Get.toNamed(Routes.SCHOOLBOOKS);
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: ColorConstants.grayf4,
                              borderRadius: BorderRadius.circular(20)),
                          child: const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              "Buy Now",
                              textScaleFactor: 1.0,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: ColorConstants.black,
                                fontSize: 14.0,
                                fontFamily: 'SofiaProMedium',
                                // fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.all(6.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: ColorConstants.yellow31,
                  borderRadius: BorderRadius.circular(8.0),
                ),
                alignment: Alignment.centerLeft,
                width: Dimensions.screenWidth,
                child: Image.network(
                    "https://w7.pngwing.com/pngs/1007/931/png-transparent-paper-office-supplies-stationery-business-material-retail-people-office-thumbnail.png"),
                // child: Image.asset(
                //   "assets/images/stationery_image.png",
                //   height: Dimensions.screenWidth / 2,
                // ),
              ),
              Positioned(
                // top: (((1.16 * Dimensions.screenWidth) / 2) - 50),
                top: 0,
                left: 0.0,
                right: 0.0,
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: const [
                      Text(
                        "School Stationery",
                        textScaleFactor: 1.0,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: ColorConstants.black,
                          fontSize: 18.0,
                          fontFamily: 'SofiaProMedium',
                          // fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                // top: (((1.16 * Dimensions.screenWidth) / 2) - 50),
                top: Dimensions.screenWidth * 0.20,
                left: 0.0,
                right: 0.0,
                child: Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      const Text(
                        "20% Off",
                        textScaleFactor: 1.0,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: ColorConstants.black,
                          fontSize: 24.0,
                          fontFamily: 'SofiaProMedium',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          // Get.toNamed(Routes.SCHOOLBOOKS);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: ColorConstants.grayf4,
                                    borderRadius: BorderRadius.circular(20)),
                                child: const Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    "Buy Now",
                                    textScaleFactor: 1.0,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: ColorConstants.black,
                                      fontSize: 14.0,
                                      fontFamily: 'SofiaProMedium',
                                      // fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],

      /// Slider Container properties
      options: CarouselOptions(
        height: ((1.16 * Dimensions.screenWidth) / 2),
        enlargeCenterPage: true,
        autoPlay: false,
        aspectRatio: 16 / 9,
        autoPlayCurve: Curves.fastOutSlowIn,
        enableInfiniteScroll: true,
        autoPlayAnimationDuration: const Duration(milliseconds: 2000),
        // viewportFraction: 0.8,
        viewportFraction: 1,
      ),
    );
  }

  Widget buildProfileDrawer() {
    return Drawer(
        child: Container(
      height: 200,
      color: Colors.green,
    ));
  }
}
