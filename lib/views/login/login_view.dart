import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/utils/app_constants.dart';
import 'package:ecommerce_bloc_demo/utils/image_paths.dart';
import 'package:ecommerce_bloc_demo/utils/ui_utils.dart';
import 'package:ecommerce_bloc_demo/views/login/bloc/login_bloc.dart';
import 'package:ecommerce_bloc_demo/views/screens.dart';
import 'package:ecommerce_bloc_demo/widgets/rounded_button.dart';
import 'package:ecommerce_bloc_demo/widgets/text_form_field_red_box.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get_utils/src/get_utils/get_utils.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  static const String routeName = '/login';

  static Route route() {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (_) => const LoginView());
  }

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  var isEmailValid = true;
  var isPasswordValid = true;
  bool isEmailEmpty = false;
  bool isPasswordEmpty = false;
  var errorMessage = "";

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Dimensions.screenWidth = MediaQuery.of(context).size.width;
    Dimensions.screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConstants.white,
        automaticallyImplyLeading: false,
        title: const Text("Stationers"),
        centerTitle: true,
      ),
      body: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is Authenticated) {
            // Navigating to the dashboard screen if the user is authenticated
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => HomeView()));
          }
          if (state is AuthError) {
            // Showing the error message if the user has entered invalid credentials
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(state.error)));
          }
        },
        child: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            if (state is Loading) {
              // Showing the loading indicator while the user is signing in
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is UnAuthenticated) {
              // Showing the sign in form if the user is not authenticated
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(12),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                bottom: 30.0,
                                left: 20.0,
                                right: 20.0,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: Dimensions.screenHeight * 0.035),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        CircleAvatar(
                                          radius: Dimensions.screenHeight / 15,
                                          backgroundImage: const AssetImage(
                                            ImagePath.splashLogo,
                                          ),
                                        ),
                                        const Padding(
                                          padding: EdgeInsets.only(
                                              top: 10.0, bottom: 10),
                                          child: Center(
                                            child: Text(
                                              "Stationers",
                                              style: TextStyle(
                                                  fontSize:
                                                      Dimensions.fontSize22,
                                                  color: ColorConstants.black,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          color: ColorConstants.appColor,
                                          height: 0.5,
                                          width: Dimensions.screenWidth / 5,
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: Dimensions.screenWidth / 22,
                                  ),
                                  const Padding(
                                    padding: EdgeInsets.only(bottom: 30.0),
                                    child: Text(
                                      "Login",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold,
                                          color: ColorConstants.black),
                                    ),
                                  ),
                                  Form(
                                    key: _formKey,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const Padding(
                                          padding:
                                              EdgeInsets.only(bottom: 12.0),
                                          child: Text(
                                            AppConstants.email,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold,
                                                color: ColorConstants.black),
                                          ),
                                        ),
                                        _EmailInput(
                                          emailController: _emailController,
                                          isEmailEmpty: isEmailEmpty,
                                        ),
                                        SizedBox(
                                          height: Dimensions.screenWidth / 24,
                                        ),
                                        const Padding(
                                          padding:
                                              EdgeInsets.only(bottom: 12.0),
                                          child: Text(
                                            AppConstants.password,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold,
                                                color: ColorConstants.black),
                                          ),
                                        ),
                                        _PasswordInput(
                                          passwordController:
                                              _passwordController,
                                          isPasswordValid: isPasswordEmpty,
                                        ),
                                        // SizedBox(
                                        //   height: Dimensions.screenWidth / 24,
                                        // ),
                                        // Row(
                                        //   mainAxisAlignment: MainAxisAlignment.end,
                                        //   children: [
                                        //     InkWell(
                                        //       onTap: () {
                                        //         // Get.toNamed(Routes.forgotPassword);
                                        //       },
                                        //       child: UiUtils().widgetText(
                                        //           AppConstants.forgotPassword,
                                        //           16,
                                        //           ColorConstants.appColor,
                                        //           // AppConstants.fontMedium,
                                        //           FontWeight.w500),
                                        //     )
                                        //   ],
                                        // ),
                                        SizedBox(
                                          height: Dimensions.screenWidth / 15,
                                        ),
                                        Center(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              _LoginButton(
                                                onTap: () {
                                                  // bool isValid = true;
                                                  //
                                                  // if (_emailController
                                                  //     .text.isEmpty) {
                                                  //   isEmailEmpty = true;
                                                  //   isValid = false;
                                                  //   errorMessage =
                                                  //       "* Required Email";
                                                  // } else if (!GetUtils.isEmail(
                                                  //     _emailController.text)) {
                                                  //   isEmailEmpty = true;
                                                  //   errorMessage =
                                                  //       "* Required Valid Email";
                                                  //   isValid = false;
                                                  // } else {
                                                  //    isValid = true;
                                                  //
                                                  //   isEmailEmpty = false;
                                                  // }
                                                  // if (_passwordController
                                                  //     .text.isEmpty) {
                                                  //   isPasswordEmpty = true;
                                                  //   errorMessage =
                                                  //       "* Required Password";
                                                  //   isValid = false;
                                                  // } else if (_passwordController
                                                  //         .text.length <
                                                  //     6) {
                                                  //   isPasswordEmpty = true;
                                                  //   errorMessage =
                                                  //       "* Password Must be more than 5 character.";
                                                  //   isValid = false;
                                                  // } else {
                                                  //    isValid = true;
                                                  //
                                                  //   isPasswordEmpty = false;
                                                  // }
                                                  // if (isValid) {
                                                  //   isEmailEmpty = false;
                                                  //   isPasswordValid = false;
                                                  //   _authenticateWithEmailAndPassword(
                                                  //       context);
                                                  // }


                                                  bool isValid = true;

                                                  if (_emailController
                                                      .text
                                                      .isEmpty) {

                                                    isEmailEmpty = true;

                                                    isValid = false;
                                                  } else {
                                                    isValid = true;
                                                    isEmailEmpty = false;
                                                  }

                                                  if (_passwordController
                                                      .text.isEmpty) {
                                                    isPasswordEmpty = true;

                                                    isValid = false;
                                                  } else if (_passwordController
                                                      .text.length <
                                                      6) {
                                                    isPasswordEmpty = true;

                                                    isValid = false;
                                                  } else {
                                                    isValid = true;

                                                    isPasswordEmpty = false;
                                                  }

                                                  if (isValid) {
                                                    isEmailEmpty = false;

                                                    isPasswordValid = false;
                                                    _authenticateWithEmailAndPassword(
                                                        context);
                                                  }








                                                },
                                              ),
                                              SizedBox(
                                                height:
                                                    Dimensions.screenWidth / 15,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    color:
                                                        ColorConstants.appColor,
                                                    height: 0.5,
                                                    width:
                                                        Dimensions.screenWidth /
                                                            6,
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 8.0,
                                                            right: 8),
                                                    child: Text(
                                                      AppConstants
                                                          .orContinueWith,
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: ColorConstants
                                                              .black),
                                                    ),
                                                  ),
                                                  Container(
                                                    color:
                                                        ColorConstants.appColor,
                                                    height: 0.5,
                                                    width:
                                                        Dimensions.screenWidth /
                                                            6,
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height:
                                                    Dimensions.screenHeight /
                                                        40,
                                              ),
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    right: 20, left: 20),
                                                child: UiUtils()
                                                    .widgetButtonWithIcon(
                                                        AppConstants
                                                            .continueWithGoogle,
                                                        ColorConstants.black,
                                                        ImagePath.googleIcon,
                                                        ColorConstants.grayfb,
                                                        ColorConstants.grayE5,
                                                        () {
                                                  _authenticateWithGoogle(
                                                      context);
                                                }),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: Dimensions.screenHeight / 40,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              UiUtils().widgetText(
                                  AppConstants.dontHaveAccount,
                                  16,
                                  ColorConstants.black,
                                  // AppConstants.fontMedium,
                                  FontWeight.w500),
                              InkWell(
                                onTap: () {
                                  Navigator.pushNamed(context, "/signup");
                                },
                                child: UiUtils().widgetText(
                                    AppConstants.signUp,
                                    16,
                                    ColorConstants.appColor,
                                    //  AppConstants.fontMedium,
                                    FontWeight.w500),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(18.0),
                //   child: SingleChildScrollView(
                //     reverse: true,
                //     child: Column(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       children: [
                //         const Text(
                //           "Sign In",
                //           style: TextStyle(
                //             fontSize: 38,
                //             fontWeight: FontWeight.bold,
                //           ),
                //         ),
                //         const SizedBox(
                //           height: 18,
                //         ),
                //         Center(
                //           child: Form(
                //             key: _formKey,
                //             child: Column(
                //               children: [
                //                 TextFormField(
                //                   keyboardType: TextInputType.emailAddress,
                //                   controller: _emailController,
                //                   decoration: const InputDecoration(
                //                     hintText: "Email",
                //                     border: OutlineInputBorder(),
                //                   ),
                //                   autovalidateMode:
                //                   AutovalidateMode.onUserInteraction,
                //                   validator: (value) {
                //                     return value != null &&
                //                         !EmailValidator.validate(value)
                //                         ? 'Enter a valid email'
                //                         : null;
                //                   },
                //                 ),
                //                 const SizedBox(
                //                   height: 10,
                //                 ),
                //                 TextFormField(
                //                   keyboardType: TextInputType.text,
                //                   controller: _passwordController,
                //                   decoration: const InputDecoration(
                //                     hintText: "Password",
                //                     border: OutlineInputBorder(),
                //                   ),
                //                   autovalidateMode:
                //                   AutovalidateMode.onUserInteraction,
                //                   validator: (value) {
                //                     return value != null && value.length < 6
                //                         ? "Enter min. 6 characters"
                //                         : null;
                //                   },
                //                 ),
                //                 const SizedBox(
                //                   height: 12,
                //                 ),
                //                 SizedBox(
                //                   width:
                //                   MediaQuery.of(context).size.width * 0.7,
                //                   child: ElevatedButton(
                //                     onPressed: () {
                //                       _authenticateWithEmailAndPassword(
                //                           context);
                //                     },
                //                     child: const Text('Sign In'),
                //                   ),
                //                 )
                //               ],
                //             ),
                //           ),
                //         ),
                //         IconButton(
                //           onPressed: () {
                //             _authenticateWithGoogle(context);
                //           },
                //           icon: Image.network(
                //             "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png",
                //             height: 30,
                //             width: 30,
                //           ),
                //         ),
                //         const Text("Don't have an account?"),
                //         OutlinedButton(
                //           onPressed: () {
                //             Navigator.pushReplacement(
                //               context,
                //               MaterialPageRoute(
                //                   builder: (context) => const SignUp()),
                //             );
                //           },
                //           child: const Text("Sign Up"),
                //         )
                //       ],
                //     ),
                //   ),
                // ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }

  void _authenticateWithEmailAndPassword(context) {
    if (_formKey.currentState!.validate()) {
      BlocProvider.of<AuthBloc>(context).add(
        SignInRequested(_emailController.text, _passwordController.text),
      );
    }
  }

  void _authenticateWithGoogle(context) {
    BlocProvider.of<AuthBloc>(context).add(
      GoogleSignInRequested(),
    );
  }
}

class _EmailInput extends StatelessWidget {
  final TextEditingController emailController;
  final bool isEmailEmpty;

  const _EmailInput({required this.emailController, required this.isEmailEmpty});

  @override
  Widget build(BuildContext context) {
    return TextFormFieldWidgetRedBox(
      key: const Key('loginForm_usernameInput_textField'),
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
      ],
      autovalidateMode: AutovalidateMode.onUserInteraction,
      enabled: true,
      controller: emailController,
      hintText: AppConstants.email,
      inputType: TextInputType.emailAddress,
      isObscureText: false,
      labelText: AppConstants.email,

      // validator: (value) {
      //   return value != null && !EmailValidator.validate(value)
      //       ? 'Enter a valid email'
      //       : null;
      // },
      // onChanged: (username) =>
      //     context.read<NewLoginBloc>().add(LoginUsernameChanged(username)
      //     ),
      isError: isEmailEmpty,
      // isBox: _onLoginController.isEmailEmpty.value,
    );
  }
}

class _PasswordInput extends StatelessWidget {
  final TextEditingController passwordController;
  final bool isPasswordValid;

  const _PasswordInput({required this.passwordController, required this.isPasswordValid});

  @override
  Widget build(BuildContext context) {
    return TextFormFieldWidgetRedBox(
      // key: const Key('loginForm_passwordInput_textField'),
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
      ],
      autovalidateMode: AutovalidateMode.onUserInteraction,
      enabled: true,
      // isObscureText: true,
      controller: passwordController,
      hintText: AppConstants.password,
      inputType: TextInputType.emailAddress,
      labelText: AppConstants.password,
      // validator: (value) {
      //   return value != null && value.length < 6
      //       ? "Enter min. 6 characters"
      //       : null;
      // },
      // passwordButton: IconButton(
      //
      //   // icon: SvgPicture.asset(
      //   //   ImagePath.pw_invisible,
      //   // ),
      //   onPressed: () {
      //     // _onLoginController.isPasswordVisible.value =
      //     // !_onLoginController
      //     //     .isPasswordVisible.value;
      //   },
      // ),
      // onChanged: (password) =>
      //     context.read<NewLoginBloc>().add(LoginPasswordChanged(password)),

      // obscureText: true,
      isError: isPasswordValid,
      // isBox: _onLoginController.isPasswordEmpty.value,
    );
  }
}

class _LoginButton extends StatelessWidget {
  final VoidCallback? onTap;

  const _LoginButton({required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 20),
      child: RoundedButton(
          onTap: onTap,
          // textcolor: ColorConstants.white,
          color: ColorConstants.appColor,
          textColor: ColorConstants.white,
          buttonName: 'Login'),
    );
  }
}
