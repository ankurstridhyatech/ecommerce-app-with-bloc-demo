import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:flutter/material.dart';

class ItemSummery extends StatefulWidget {
  const ItemSummery({
    Key? key,
    this.product,
    this.quantity,
  }) : super(key: key);
  final CategoryProduct? product;
  final int? quantity;

  @override
  State<ItemSummery> createState() => _ItemSummeryState();
}

class _ItemSummeryState extends State<ItemSummery> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      child: Dismissible(
          key: UniqueKey(),
          direction: DismissDirection.endToStart,
          onDismissed: (direction) {
            setState(() {

            });
          },
          background: Container(
            color: Theme.of(context).errorColor,
            alignment: Alignment.centerRight,
            padding: const EdgeInsets.only(right: 20),
            margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 4),
            child: const Icon(
              Icons.delete,
              color: Colors.white,
              size: 40,
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: 80,
                width: 80,
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(60),
                    topRight: Radius.circular(60),
                    bottomLeft: Radius.circular(60),
                    bottomRight: Radius.circular(60),
                  ),
                  child: Image.network(
                    widget.product!.imageUrl ?? "",
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: Dimensions.screenWidth * 0.33,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.product!.title ?? "",
                              style: const TextStyle(
                                  color: Colors.black, fontSize: 16),
                              maxLines: 2,
                            ),
                            const SizedBox(height: 10),
                            Row(
                              children: [

                                Text.rich(
                                  TextSpan(
                                    text: "Qty.",
                                    style: const TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: ColorConstants.black,
                                        fontSize: 18),
                                    children: [
                                      TextSpan(
                                          text: "  " "${widget.quantity}",
                                          style: const TextStyle(
                                              fontWeight: FontWeight.w400,
                                              color: ColorConstants.black,
                                              fontSize: 18),)
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        // color: Colors.green,
                        width: Dimensions.screenWidth * 0.02,
                      ),
                      Chip(
                        backgroundColor: ColorConstants.appColor,
                        label: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Row(
                            children: [
                              Text(
                                '${(widget.product!.price ?? "" * 1)}',
                                style: const TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),

                ],
              ),
            ],
          )),
    );
  }

  SizedBox buildOutlineButton({IconData? icon, VoidCallback? press}) {
    return SizedBox(
      width: 30,
      height: 30,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          padding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        onPressed: press,
        child: Icon(
          icon,
          size: 18,
        ),
      ),
    );
  }
}
