import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/utils/image_paths.dart';
import 'package:ecommerce_bloc_demo/views/cart/bloc/cart_bloc.dart';
import 'package:ecommerce_bloc_demo/views/order_confirmation/Components/item_summery_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OrderConfirmationView extends StatelessWidget {
  const OrderConfirmationView({Key? key}) : super(key: key);
  static const String routeName = '/orderConfirmation';

  static Route route() {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (_) => const OrderConfirmationView());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorConstants.white,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: ColorConstants.appColor,
          title: const Text(
            "Order Confirmation ",
            style: TextStyle(color: Colors.white),
          ),
          actions: [
            InkWell(
              splashFactory: NoSplash.splashFactory,
              highlightColor: ColorConstants.transparent,
              onTap: () {
                Navigator.pushNamed(context, '/wishlist');
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: SizedBox(
                  height: 35,
                  width: 35,
                  child: Image.asset(
                    ImagePath.wishlist,
                    height: 20,
                    width: 20,
                    color: ColorConstants.black,
                  ),
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: bottomNavWidget(context),
        body: BlocBuilder<CartBloc, CartState>(builder: (context, state) {
          if (state is CartLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is CartLoadedState) {
            return state.cart!
                    .productQuantity(state.cart!.products)!
                    .keys
                    .isNotEmpty
                ? SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: double.infinity,
                                height: Dimensions.screenHeight * 0.4,
                                decoration: BoxDecoration(
                                    color: ColorConstants.greenBB,
                                    borderRadius: BorderRadius.circular(20)),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 0.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Text(
                                        'Thank you for your order !',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: Dimensions.fontSize22,
                                            fontWeight: FontWeight.bold,
                                            color: ColorConstants.green33),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 20.0),
                                        child: CircleAvatar(
                                          radius: Dimensions.screenHeight / 10,
                                          backgroundImage: const AssetImage(
                                            ImagePath.splashLogo,
                                          ),
                                        ),
                                      ),
                                      const Text(
                                        'Your order is complete!',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: Dimensions.fontSize20,
                                            // fontWeight: FontWeight.bold,
                                            color: ColorConstants.green33),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 20.0, bottom: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(height: 10),
                                const Text(
                                  'ORDER CODE: #k321-ekd3',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17),
                                ),
                                BlocBuilder<CartBloc, CartState>(
                                    builder: (context, state) {
                                  if (state is CartLoadingState) {
                                    return const Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  if (state is CartLoadedState) {
                                    return Padding(
                                      padding: const EdgeInsets.only(
                                          top: 20.0, left: 20, right: 20),
                                      child: Container(
                                        //color: Colors.deepOrange,
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 16),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: ColorConstants.grayf4,
                                        ),
                                        child: Column(
                                          //  crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 0.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  const Text(
                                                    "SubTotal",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                  Text(
                                                    "\u{20B9} ${state.cart!.subtotalString}",
                                                    style: const TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 18),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  const Text(
                                                    "Delivery Fee",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                  Text(
                                                    "\u{20B9} ${state.cart!.deliveryFeeString}",
                                                    style: const TextStyle(
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 18),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 8.0),
                                              child: Container(
                                                // padding: EdgeInsets.all(8),
                                                height: 1,
                                                color: ColorConstants.grayB5,
                                              ),
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                const Text(
                                                  "Total amount",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Text(
                                                  "\u{20B9} ${state.cart!.totalString}",
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 20),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  }
                                  if (state is CartErrorState) {
                                    return buildError(state.message);
                                  }
                                  return Container();
                                }),

                                // OrderSummary(),
                                const SizedBox(height: 20),
                                const Text(
                                  'ORDER DETAILS',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17),
                                ),
                                const Divider(thickness: 2),
                                const SizedBox(height: 20),
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 16),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: ColorConstants.grayf4,
                                  ),
                                  child: ListView.separated(
                                    shrinkWrap: true,
                                    physics: const ScrollPhysics(),
                                    itemCount: state.cart!
                                        .productQuantity(state.cart!.products)!
                                        .keys
                                        .length,
                                    itemBuilder: (context, index) =>
                                        ItemSummery(
                                      product: state.cart!
                                          .productQuantity(
                                              state.cart!.products)!
                                          .keys
                                          .elementAt(index),
                                      quantity: state.cart!
                                          .productQuantity(
                                              state.cart!.products)!
                                          .values
                                          .elementAt(index),
                                    ),
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return const Padding(
                                        padding: EdgeInsets.only(
                                            left: 8.0, right: 8),
                                        child: Divider(thickness: 0.5),
                                      );
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : Container();
          }
          if (state is CartErrorState) {
            return buildError(state.message);
          }
          return Container();
        }));
  }

  Widget bottomNavWidget(BuildContext context) {
    return BottomAppBar(
        child: Container(
      width: Dimensions.screenWidth,
      // height: 60,
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: TextButton(
        style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(60)),
          primary: Colors.white,
          backgroundColor: ColorConstants.appColor,
        ),
        onPressed: () {
          Navigator.pushNamed(context, "/home");
        },
        child: const Padding(
          padding: EdgeInsets.symmetric(vertical: 5.0),
          child: Text(
            "Back To Shopping",
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
        ),
      ),
    ));
  }

  Widget buildError(String message) {
    return Center(
      child: Text(message),
    );
  }
}
