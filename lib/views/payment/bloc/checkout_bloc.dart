import 'dart:async';

import 'package:ecommerce_bloc_demo/model/models.dart';
import 'package:ecommerce_bloc_demo/repository/checkout/checkout_repository.dart';
import 'package:ecommerce_bloc_demo/views/cart/bloc/cart_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'checkout_event.dart';

part 'checkout_state.dart';

class CheckoutBloc extends Bloc<CheckoutEvent, CheckoutState> {
  final CartBloc? cartBloc;
  final CheckoutRepository? _checkoutRepository;
  StreamSubscription? _cartSubscription;
  StreamSubscription? _checkoutSubscription;

  CheckoutBloc(
      {required CartBloc cartBloc,
      required CheckoutRepository checkoutRepository})
      : cartBloc = cartBloc,
        _checkoutRepository = checkoutRepository,
        super(cartBloc.state is CartLoadedState
            ? CheckoutLoadedState(
                products: (cartBloc.state as CartLoadedState).cart!.products,
                subTotal:
                    (cartBloc.state as CartLoadedState).cart!.subtotalString,
                total: (cartBloc.state as CartLoadedState).cart!.totalString,
                deliveryFee:
                    (cartBloc.state as CartLoadedState).cart!.deliveryFeeString,
              )
            : CheckoutLoadingState()) {
    on<UpdateCheckout>((event, emit) async {
      final state = this.state as CheckoutLoadedState;
      ; // local variable

      // emit(CheckoutLoadingState());
      try {
        emit(CheckoutLoadedState(
          email: event.email ?? state.email,
          fullName: event.fullName ?? state.fullName,
          products: event.cart?.products ?? state.products,
          deliveryFee: event.cart?.deliveryFeeString ?? state.deliveryFee,
          subTotal: event.cart?.subtotalString ?? state.subTotal,
          total: event.cart?.totalString ?? state.total,
          address: event.address ?? state.address,
          city: event.city ?? state.city,
          country: event.country ?? state.country,
          zipCode: event.zipCode ?? state.zipCode,
        ));
      } catch (e) {
        emit(CheckoutErrorState(
          message: e.toString(),
        ));
      }
    });

    on<ConfirmCheckout>((ConfirmCheckout event, emit) async {
      _checkoutSubscription?.cancel();

      if (state is CheckoutLoadedState) {
        try {
          await _checkoutRepository!.addCheckout(event.checkout!);
          if (kDebugMode) {
            print('Done');
          }
          emit(CheckoutLoadingState());
        } catch (e) {
          emit(CheckoutErrorState(
            message: e.toString(),
          ));
        }
      }
    });

    _cartSubscription = cartBloc.stream.listen(
      (state) {
        if (state is CartLoadedState) {
          add(
            UpdateCheckout(cart: state.cart),
          );
        }
      },
    );
  }

  @override
  Future<void> close() {
    _cartSubscription?.cancel();
    return super.close();
  }
}
