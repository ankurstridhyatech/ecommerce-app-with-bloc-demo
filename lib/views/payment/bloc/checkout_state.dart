part of 'checkout_bloc.dart';

abstract class CheckoutState extends Equatable {
  const CheckoutState();

  @override
  List<Object?> get props => [];
}

class CheckoutInitialState extends CheckoutState {
  @override
  List<Object?> get props => [];
}

class CheckoutLoadingState extends CheckoutState {
  @override
  List<Object?> get props => [];
}

class CheckoutLoadedState extends CheckoutState {
  final String? fullName;
  final String? email;
  final String? address;
  final String? city;
  final String? country;
  final String? zipCode;
  final String? subTotal;
  final String? deliveryFee;
  final String? total;
  final List<CategoryProduct>? products;
  final Checkout? checkout;
  final PaymentMethod paymentMethod;

  CheckoutLoadedState({
    this.fullName,
    this.email,
    this.address,
    this.city,
    this.country,
    this.zipCode,
    this.subTotal,
    this.deliveryFee,
    this.products,
    this.total,
    this.paymentMethod = PaymentMethod.google_pay,
  }) : checkout = Checkout(
            fullName: fullName,
            email: email,
            address: address,
            city: city,
            country: country,
            zipCode: zipCode,
            subTotal: subTotal,
            deliveryFee: deliveryFee,
            products: products,
            total: total);

  @override
  List<Object?> get props => [
        fullName,
        email,
        address,
        city,
        country,
        zipCode,
        subTotal,
        deliveryFee,
        products,
        total,
        paymentMethod,
      ];
}

class CheckoutErrorState extends CheckoutState {
  final String message;

  const CheckoutErrorState({required this.message});

  @override
  List<Object?> get props => [message];
}
