import 'dart:io';

import 'package:ecommerce_bloc_demo/model/payment_method_model.dart';
import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/views/cart/bloc/cart_bloc.dart';
import 'package:ecommerce_bloc_demo/views/payment/bloc/checkout_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PaymentView extends StatefulWidget {
  const PaymentView({Key? key, required this.total}) : super(key: key);

  final String total;

  static const String routeName = '/payment';

  static Route route() {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (_) => const PaymentView(
              total: '',
            ));
  }

  @override
  State<PaymentView> createState() => _PaymentViewState();
}

class _PaymentViewState extends State<PaymentView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBar(),
        body:
            BlocBuilder<CheckoutBloc, CheckoutState>(builder: (context, state) {
          if (state is CheckoutLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is CheckoutLoadedState) {
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 8,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text(
                          "Payment",
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        // Text(
                        //   "Order number is 1235462412",
                        //   style: TextStyle(
                        //     fontSize: 10,
                        //     color: ColorConstants.gray1C,
                        //   ),
                        // )
                      ],
                    ),
                  ),
                  const Divider(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'CUSTOMER INFORMATION',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        CustomTextFormField(
                          title: 'Email',
                          onChanged: (value) {
                            context
                                .read<CheckoutBloc>()
                                .add(UpdateCheckout(email: value));
                          },
                        ),
                        CustomTextFormField(
                          title: 'Full Name',
                          onChanged: (value) {
                            context
                                .read<CheckoutBloc>()
                                .add(UpdateCheckout(fullName: value));
                          },
                        ),
                        const SizedBox(height: 20),
                        const Text(
                          'DELIVERY INFORMATION',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        CustomTextFormField(
                          title: 'Address',
                          onChanged: (value) {
                            context
                                .read<CheckoutBloc>()
                                .add(UpdateCheckout(address: value));
                          },
                        ),
                        CustomTextFormField(
                          title: 'City',
                          onChanged: (value) {
                            context
                                .read<CheckoutBloc>()
                                .add(UpdateCheckout(city: value));
                          },
                        ),
                        CustomTextFormField(
                          title: 'Country',
                          onChanged: (value) {
                            context
                                .read<CheckoutBloc>()
                                .add(UpdateCheckout(country: value));
                          },
                        ),
                        CustomTextFormField(
                          title: 'ZIP Code',
                          onChanged: (value) {
                            context
                                .read<CheckoutBloc>()
                                .add(UpdateCheckout(zipCode: value));
                          },
                        ),
                      ],
                    ),
                  ),

                  const SizedBox(height: 20),
                  Container(
                    height: 180,
                    margin:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                    width: double.infinity,
                    padding: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      gradient: const SweepGradient(
                          center: AlignmentDirectional(1, -1),
                          startAngle: 1.7,
                          endAngle: 3,
                          colors: <Color>[
                            Color(0xff148535),
                            Color(0xff148535),
                            Color(0xff0D6630),
                            Color(0xff0D6630),
                            Color(0xff148535),
                            Color(0xff148535),
                          ],
                          stops: <double>[
                            0.0,
                            0.3,
                            0.3,
                            0.7,
                            0.7,
                            1.0
                          ]),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: const [
                            Text(
                              "VISA",
                              style: TextStyle(
                                fontSize: 24.30,
                                fontWeight: FontWeight.bold,
                                color: ColorConstants.white,
                              ),
                            ),
                            Text(
                              "visa Electron",
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: ColorConstants.white,
                              ),
                            )
                          ],
                        ),
                        const Text(
                          "**** **** **** **** 1256",
                          style: TextStyle(
                            fontSize: 24.30,
                            fontWeight: FontWeight.bold,
                            color: ColorConstants.white,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: const [
                                      Text(
                                        "Card holder",
                                        style: TextStyle(
                                          fontSize: 12,
                                          color: ColorConstants.white,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "Aqeel Baloch",
                                        style: TextStyle(
                                          fontSize: 12,
                                          color: ColorConstants.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: const [
                                      Text(
                                        "Expries",
                                        style: TextStyle(
                                          fontSize: 12,
                                          color: ColorConstants.white,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "2/21",
                                        style: TextStyle(
                                          fontSize: 12,
                                          color: ColorConstants.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const Expanded(
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: CircleAvatar(
                                      backgroundColor: ColorConstants.green2D,
                                      child: Icon(
                                        Icons.check,
                                        color: ColorConstants.white,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, "/paymentSelection");
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 8.0, bottom: 8.0, left: 20.0, right: 20.0),
                      child: Container(
                        // height: 40,
                        width: Dimensions.screenWidth,
                        decoration: BoxDecoration(
                            color: ColorConstants.grayE5,
                            // image: DecorationImage(
                            //     image: AssetImage(PaymentLabels
                            //         .paymentLabels[index]
                            //         .imageUrl!)),
                            borderRadius: BorderRadius.circular(12)),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: const [
                              Text(
                                "Payment Selection Method",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Icon(CupertinoIcons.chevron_forward)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  // BlocBuilder<PaymentBloc, PaymentState>(builder: (context, state) {
                  //   if (state is PaymentLoadingState) {
                  //     return const Center(
                  //       child: CircularProgressIndicator(),
                  //     );
                  //   }
                  //   if (state is PaymentLoadedState) {
                  //     return ListView.separated(
                  //       itemCount: PaymentLabels.paymentLabels.length,
                  //       shrinkWrap: true,
                  //       physics: const NeverScrollableScrollPhysics(),
                  //       itemBuilder: (context, index) {
                  //         return Padding(
                  //           padding: const EdgeInsets.only(left: 20.0, right: 20),
                  //           child: Row(
                  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //             children: [
                  //               Row(
                  //                 children: [
                  //                   Padding(
                  //                     padding: const EdgeInsets.only(
                  //                         top: 8.0, bottom: 8.0, left: 16.0),
                  //                     child: Container(
                  //                       height: 40,
                  //                       width: 70,
                  //                       decoration: BoxDecoration(
                  //                           color: ColorConstants.grayE5,
                  //                           image: DecorationImage(
                  //                               image: AssetImage(PaymentLabels
                  //                                   .paymentLabels[index]
                  //                                   .imageUrl!)),
                  //                           borderRadius:
                  //                               BorderRadius.circular(12)),
                  //                     ),
                  //                   ),
                  //                   Padding(
                  //                     padding: const EdgeInsets.only(left: 14.0),
                  //                     child: Text(
                  //                       PaymentLabels.paymentLabels[index].title!,
                  //                       style: const TextStyle(
                  //                           color: ColorConstants.black44,
                  //                           fontWeight: FontWeight.bold,
                  //                           fontSize: 16),
                  //                     ),
                  //                   ),
                  //                 ],
                  //               ),
                  //               Radio(
                  //                 activeColor: ColorConstants.appColor,
                  //                 value: index,
                  //                 groupValue: value,
                  //                 onChanged: ((i) =>
                  //                     setState(() => value = i as int?)),
                  //                 // onChanged: (i) => setState(() => value = i),
                  //               ),
                  //             ],
                  //           ),
                  //         );
                  //
                  //         //   ListTile(
                  //         //   leading: Radio(
                  //         //     activeColor: ColorConstants.appColor,
                  //         //     value: index,
                  //         //     groupValue: value,
                  //         //     onChanged: ((i) => setState(() => value = i as int?)),
                  //         //     // onChanged: (i) => setState(() => value = i),
                  //         //   ),
                  //         //   title: Text(
                  //         //     PaymentLabels.paymentLabels[index].title!,
                  //         //     style: const TextStyle(color: ColorConstants.black),
                  //         //   ),
                  //         //   // trailing: Icon(paymentIcons[index], color: ColorConstants.appColor),
                  //         // );
                  //       },
                  //       separatorBuilder: (context, index) {
                  //         return const Divider();
                  //       },
                  //     );
                  //   }
                  //   if (state is PaymentErrorState) {
                  //     return Center(
                  //       child: Text(state.message),
                  //     );
                  //   }
                  //   return Container();
                  // }),
                  const SizedBox(
                    height: 18,
                  ),
                  BlocBuilder<CartBloc, CartState>(
                    builder: (context, state) {
                      if (state is CartLoadedState) {
                        return Padding(
                          padding: const EdgeInsets.only(
                              left: 20.0, right: 20, bottom: 20),
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 16),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: ColorConstants.grayf4,
                            ),
                            child: Column(
                              //  crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 0.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text(
                                        "SubTotal",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400),
                                      ),
                                      Text(
                                        "\u{20B9} ${state.cart!.subtotalString}",
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 18),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text(
                                        "Delivery Fee",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400),
                                      ),
                                      Text(
                                        "\u{20B9} ${state.cart!.deliveryFeeString}",
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 18),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Container(
                                    // padding: EdgeInsets.all(8),
                                    height: 1,
                                    color: ColorConstants.grayB5,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Text(
                                      "Total amount",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "\u{20B9} ${state.cart!.totalString}",
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      }
                      return Container();
                    },
                  ),

                  const SizedBox(
                    height: 80,
                  )
                ],
              ),
            );
          }
          if (state is CheckoutErrorState) {
            return Center(
              child: Text(state.message),
            );
          }
          return Container();
        }),
        bottomSheet: const OrderNowNavBar());
  }

  Widget buildTextField({
    double? leftPadding,
    double? rightPadding,
    String? hintText,
  }) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 20,
        top: 20,
        left: leftPadding!,
        right: rightPadding!,
      ),
      child: TextField(
        decoration: InputDecoration(
          hintText: hintText,
          fillColor: Colors.grey[100],
          filled: true,
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: ColorConstants.appColor,
      elevation: 0,
      leading: const SizedBox(),
      // On Android it's false by default
      centerTitle: true,
      title: const Text("Checkout"),
    );
  }
}

class OrderNowNavBar extends StatelessWidget {
  const OrderNowNavBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        BlocBuilder<CheckoutBloc, CheckoutState>(
          builder: (context, state) {
            if (state is CheckoutLoadingState) {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.white,
                ),
              );
            }
            if (state is CheckoutLoadedState) {
              // return Container(
              //   height: 60,
              //   width: Dimensions.screenWidth,
              //   child: GooglePay(
              //     products: state.products!,
              //     total: state.total!,
              //   ),
              // );
              if (state.paymentMethod == PaymentMethod.credit_card) {
                return Container(
                  width: double.infinity,
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: TextButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60)),
                      primary: Colors.white,
                      backgroundColor: ColorConstants.appColor,
                    ),
                    onPressed: () {
                      // Navigator.pushNamed(
                      //   context,
                      //   '/paymentSelection',
                      // );
                    },
                    child: const Padding(
                      padding: EdgeInsets.symmetric(vertical: 5.0),
                      child: Text(
                        "Pay with Credit Card",
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                );
              }
              if (Platform.isAndroid &&
                  state.paymentMethod == PaymentMethod.google_pay) {
                return Container(
                  width: Dimensions.screenWidth,
                  // height: 60,
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: TextButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60)),
                      primary: Colors.white,
                      backgroundColor: ColorConstants.appColor,
                    ),
                    onPressed: () {
                      context
                          .read<CheckoutBloc>()
                          .add(ConfirmCheckout(checkout: state.checkout));
                      Navigator.pushNamed(context, "/orderConfirmation");
                    },
                    child: const Padding(
                      padding: EdgeInsets.symmetric(vertical: 5.0),
                      child: Text(
                        "Pay",
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                );
                //   GooglePay(
                //   products: state.products!,
                //   total: state.total!,
                // );
              } else {
                return Container(
                  width: double.infinity,
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: TextButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(60)),
                      primary: Colors.white,
                      backgroundColor: ColorConstants.appColor,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(
                        context,
                        '/paymentSelection',
                      );
                    },
                    child: const Padding(
                      padding: EdgeInsets.symmetric(vertical: 5.0),
                      child: Text(
                        "Pay",
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                );
              }
            } else {
              return const Text('Something went wrong');
            }
          },
        ),
      ],
    );
  }
}

class Info extends StatelessWidget {
  const Info({
    Key? key,
    this.name,
    this.email,
    this.image,
  }) : super(key: key);
  final String? name, email, image;

  @override
  Widget build(BuildContext context) {
    //   double defaultSize = SizeConfig.defaultSize;
    return SizedBox(
      height: Dimensions.screenWidth * 0.024 * 24, // 240
      child: Stack(
        children: <Widget>[
          ClipPath(
            clipper: CustomShape(),
            child: Container(
              height: Dimensions.screenWidth * 0.024 * 15, //150
              color: ColorConstants.appColor,
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      bottom: Dimensions.screenWidth * 0.024), //10
                  height: Dimensions.screenWidth * 0.024 * 14, //140
                  width: Dimensions.screenWidth * 0.024 * 14,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Colors.white,
                      width: Dimensions.screenWidth * 0.024 * 0.8, //8
                    ),
                    image: const DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage("assets/images/book_wallpaper.jpg"),
                    ),
                  ),
                ),
                Text(
                  name!,
                  style: TextStyle(
                    fontSize: Dimensions.screenWidth * 0.024 * 2.2, // 22
                    color: ColorConstants.black,
                  ),
                ),
                SizedBox(height: Dimensions.screenWidth * 0.024 / 2), //5
                Text(
                  email!,
                  style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF8492A2),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class CustomShape extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    double height = size.height;
    double width = size.width;
    path.lineTo(0, height - 100);
    path.quadraticBezierTo(width / 2, height, width, height - 100);
    path.lineTo(width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class ProfileMenuItem extends StatelessWidget {
  const ProfileMenuItem({
    Key? key,
    this.iconSrc,
    this.title,
    this.press,
  }) : super(key: key);
  final String? iconSrc, title;
  final VoidCallback? press;

  @override
  Widget build(BuildContext context) {
    double defaultSize = Dimensions.screenWidth * 0.024;
    return InkWell(
      onTap: press,
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: defaultSize * 2, vertical: defaultSize * 3),
        child: SafeArea(
          child: Row(
            children: <Widget>[
              SvgPicture.asset(
                iconSrc!,
                height: 20,
                width: 20,
                color: ColorConstants.black,
              ),
              SizedBox(width: defaultSize * 2),
              Text(
                title!,
                style: TextStyle(
                  fontSize: defaultSize * 1.6, //16
                  color: ColorConstants.black,
                ),
              ),
              const Spacer(),
              Icon(
                Icons.arrow_forward_ios,
                size: defaultSize * 1.6,
                color: ColorConstants.black,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class PaymentLabels {
  final String? title;
  final String? imageUrl;

  const PaymentLabels({
    this.title,
    this.imageUrl,
  });

  List<Object?> get props => [
        title,
        imageUrl,
      ];

  static List<PaymentLabels> paymentLabels = [
    const PaymentLabels(
      title: 'G Pay',
      imageUrl: 'assets/icons/google_pay.png',
    ),
    const PaymentLabels(
        title: 'Apple Pay', imageUrl: 'assets/icons/apple_pay.png'),
  ];
}

class CustomTextFormField extends StatelessWidget {
  const CustomTextFormField({
    Key? key,
    required this.title,
    this.onChanged,
  }) : super(key: key);

  final String title;
  final Function(String)? onChanged;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        children: [
          SizedBox(
            width: 75,
            child: Text(
              title,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          Expanded(
            child: TextFormField(
              onChanged: onChanged,
              decoration: const InputDecoration(
                isDense: true,
                contentPadding: EdgeInsets.only(left: 10),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
