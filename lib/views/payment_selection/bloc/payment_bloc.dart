import 'package:ecommerce_bloc_demo/model/payment_method_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'payment_event.dart';

part 'payment_state.dart';

class PaymentBloc extends Bloc<PaymentEvent, PaymentState> {
  PaymentBloc() : super(PaymentLoadingState()) {
    on<LoadPaymentMethod>((event, emit) async {
      await Future<void>.delayed(const Duration(seconds: 1));
      emit(PaymentLoadingState());
      try {
        emit(const PaymentLoadedState());
      } catch (e) {
        emit(PaymentErrorState(
          message: e.toString(),
        ));
      }
    });

    on<SelectPaymentMethod>((SelectPaymentMethod event, emit) async {
      final state = this.state; // local variable

      if (state is PaymentLoadedState) {
        try {
          emit(PaymentLoadedState(paymentMethod: event.paymentMethod));
        } catch (e) {
          emit(PaymentErrorState(
            message: e.toString(),
          ));
        }
      }
    });
  }
}
