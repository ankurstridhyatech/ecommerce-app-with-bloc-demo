
part of 'payment_bloc.dart';

// import 'package:ecommerce_bloc_demo/model/product.dart';

abstract class PaymentEvent extends Equatable {
  const PaymentEvent();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class LoadPaymentMethod extends PaymentEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SelectPaymentMethod extends PaymentEvent {
  final PaymentMethod? paymentMethod;

  const SelectPaymentMethod({this.paymentMethod});

  @override
  // TODO: implement props
  List<Object> get props => [paymentMethod!];
}

