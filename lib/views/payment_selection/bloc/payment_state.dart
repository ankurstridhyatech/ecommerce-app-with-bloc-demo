part of 'payment_bloc.dart';

abstract class PaymentState extends Equatable {
  const PaymentState();

  @override
  List<Object> get props => [];
}

class PaymentInitialState extends PaymentState {
  @override
  List<Object> get props => [];
}

class PaymentLoadingState extends PaymentState {
  @override
  List<Object> get props => [];
}

class PaymentLoadedState extends PaymentState {
  final PaymentMethod? paymentMethod;

  const PaymentLoadedState({this.paymentMethod =  PaymentMethod.google_pay});

  @override
  List<Object> get props => [paymentMethod!];
}

class PaymentErrorState extends PaymentState {
  final String message;

  const PaymentErrorState({required this.message});

  @override
  List<Object> get props => [message];
}
