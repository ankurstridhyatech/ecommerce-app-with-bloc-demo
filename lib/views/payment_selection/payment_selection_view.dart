import 'package:ecommerce_bloc_demo/model/payment_method_model.dart';
import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/views/payment_selection/bloc/payment_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PaymentSelectionView extends StatefulWidget {
  const PaymentSelectionView({
    Key? key,
  }) : super(key: key);

  static const String routeName = '/paymentSelection';

  static Route route() {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (_) => const PaymentSelectionView());
  }

  @override
  State<PaymentSelectionView> createState() => _PaymentSelectionViewState();
}

class _PaymentSelectionViewState extends State<PaymentSelectionView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 8,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text(
                    "Payment Selection Method",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),

                ],
              ),
            ),
            const Divider(),
            BlocBuilder<PaymentBloc, PaymentState>(builder: (context, state) {
              if (state is PaymentLoadingState) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (state is PaymentLoadedState) {
                return ListView.separated(
                  itemCount: PaymentLabels.paymentLabels.length,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        PaymentLabels.paymentLabels[index] == 0
                            ? context.read<PaymentBloc>().add(
                                const SelectPaymentMethod(
                                    paymentMethod: PaymentMethod.google_pay))
                            : context.read<PaymentBloc>().add(
                                const SelectPaymentMethod(
                                    paymentMethod: PaymentMethod.apple_pay));
                        Navigator.pop(context);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 8.0, bottom: 8.0, left: 0.0),
                            child: Container(
                              height: 50,
                              width: Dimensions.screenWidth,
                              decoration: BoxDecoration(
                                  color: ColorConstants.grayE5,
                                  image: DecorationImage(
                                      image: AssetImage(PaymentLabels
                                          .paymentLabels[index].imageUrl!)),
                                  borderRadius: BorderRadius.circular(12)),
                            ),
                          ),
                        ],
                      ),
                    );


                  },
                  separatorBuilder: (context, index) {
                    return const Divider();
                  },
                );
              }
              if (state is PaymentErrorState) {
                return Center(
                  child: Text(state.message),
                );
              }
              return Container();
            }),
            const SizedBox(
              height: 18,
            ),
          ],
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: ColorConstants.appColor,
      elevation: 0,
      leading: const SizedBox(),
      // On Android it's false by default
      centerTitle: true,
      title: const Text("Payment Selection Method"),
    );
  }
}
//
// class Info extends StatelessWidget {
//   const Info({
//     Key? key,
//     this.name,
//     this.email,
//     this.image,
//   }) : super(key: key);
//   final String? name, email, image;
//
//   @override
//   Widget build(BuildContext context) {
//     //   double defaultSize = SizeConfig.defaultSize;
//     return SizedBox(
//       height: Dimensions.screenWidth * 0.024 * 24, // 240
//       child: Stack(
//         children: <Widget>[
//           ClipPath(
//             clipper: CustomShape(),
//             child: Container(
//               height: Dimensions.screenWidth * 0.024 * 15, //150
//               color: ColorConstants.appColor,
//             ),
//           ),
//           Center(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.end,
//               children: <Widget>[
//                 Container(
//                   margin: EdgeInsets.only(
//                       bottom: Dimensions.screenWidth * 0.024), //10
//                   height: Dimensions.screenWidth * 0.024 * 14, //140
//                   width: Dimensions.screenWidth * 0.024 * 14,
//                   decoration: BoxDecoration(
//                     shape: BoxShape.circle,
//                     border: Border.all(
//                       color: Colors.white,
//                       width: Dimensions.screenWidth * 0.024 * 0.8, //8
//                     ),
//                     image: const DecorationImage(
//                       fit: BoxFit.cover,
//                       image: AssetImage("assets/images/book_wallpaper.jpg"),
//                     ),
//                   ),
//                 ),
//                 Text(
//                   name!,
//                   style: TextStyle(
//                     fontSize: Dimensions.screenWidth * 0.024 * 2.2, // 22
//                     color: ColorConstants.black,
//                   ),
//                 ),
//                 SizedBox(height: Dimensions.screenWidth * 0.024 / 2), //5
//                 Text(
//                   email!,
//                   style: const TextStyle(
//                     fontWeight: FontWeight.w400,
//                     color: Color(0xFF8492A2),
//                   ),
//                 )
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
//
// class CustomShape extends CustomClipper<Path> {
//   @override
//   Path getClip(Size size) {
//     var path = Path();
//     double height = size.height;
//     double width = size.width;
//     path.lineTo(0, height - 100);
//     path.quadraticBezierTo(width / 2, height, width, height - 100);
//     path.lineTo(width, 0);
//     path.close();
//     return path;
//   }
//
//   @override
//   bool shouldReclip(CustomClipper<Path> oldClipper) {
//     return true;
//   }
// }
//
// class ProfileMenuItem extends StatelessWidget {
//   const ProfileMenuItem({
//     Key? key,
//     this.iconSrc,
//     this.title,
//     this.press,
//   }) : super(key: key);
//   final String? iconSrc, title;
//   final VoidCallback? press;
//
//   @override
//   Widget build(BuildContext context) {
//     double defaultSize = Dimensions.screenWidth * 0.024;
//     return InkWell(
//       onTap: press,
//       child: Padding(
//         padding: EdgeInsets.symmetric(
//             horizontal: defaultSize * 2, vertical: defaultSize * 3),
//         child: SafeArea(
//           child: Row(
//             children: <Widget>[
//               SvgPicture.asset(
//                 iconSrc!,
//                 height: 20,
//                 width: 20,
//                 color: ColorConstants.black,
//               ),
//               SizedBox(width: defaultSize * 2),
//               Text(
//                 title!,
//                 style: TextStyle(
//                   fontSize: defaultSize * 1.6, //16
//                   color: ColorConstants.black,
//                 ),
//               ),
//               const Spacer(),
//               Icon(
//                 Icons.arrow_forward_ios,
//                 size: defaultSize * 1.6,
//                 color: ColorConstants.black,
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

class PaymentLabels {
  final String? title;
  final String? imageUrl;

  const PaymentLabels({
    this.title,
    this.imageUrl,
  });

  List<Object?> get props => [
        title,
        imageUrl,
      ];

  static List<PaymentLabels> paymentLabels = [
    const PaymentLabels(
      title: 'G Pay',
      imageUrl: 'assets/icons/google_pay.png',
    ),
    const PaymentLabels(
        title: 'Apple Pay', imageUrl: 'assets/icons/apple_pay.png'),
  ];
}
