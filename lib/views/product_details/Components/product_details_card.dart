import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProductDetailsCard extends StatelessWidget {
  final String? name;
  final String? imgUrl;

  const ProductDetailsCard({Key? key, this.name, this.imgUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Dimensions.screenHeight * 0.25,
      // width: 60,
      decoration: BoxDecoration(
          color: ColorConstants.white, borderRadius: BorderRadius.circular(10)),
      child: Container(
        margin: const EdgeInsets.all(6.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                color: ColorConstants.greendb,
                borderRadius: BorderRadius.circular(8.0),
              ),
              alignment: Alignment.bottomLeft,
              width: Dimensions.screenWidth,
              child: SizedBox(
                height: Dimensions.screenHeight * 0.18,
                width: Dimensions.screenWidth * 0.5,
                child: Image.network(imgUrl!),
              ),
            ),
            Positioned(
              top: 0,
              left: 0.0,
              right: 0.0,
              child: Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      name!,
                      textScaleFactor: 1.0,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        color: ColorConstants.black,
                        fontSize: 18.0,
                        fontFamily: 'SofiaProMedium',
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: Dimensions.screenWidth * 0.20,
              left: 0.0,
              right: 0.0,
              child: InkWell(
                onTap: () {},
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: ColorConstants.grayf4,
                            borderRadius: BorderRadius.circular(20)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SvgPicture.asset(
                            "assets/icons/arrow_next.svg",
                            color: ColorConstants.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
