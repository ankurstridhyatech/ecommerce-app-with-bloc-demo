import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/utils/image_paths.dart';
import 'package:ecommerce_bloc_demo/views/cart/bloc/cart_bloc.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_bloc.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_event.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductDetailsView extends StatelessWidget {
  const ProductDetailsView({Key? key, required this.product}) : super(key: key);

  final CategoryProduct product;

  static const String routeName = '/category';

  static Route route({required CategoryProduct product}) {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (_) => ProductDetailsView(
              product: product,
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: ColorConstants.appColor,
        title: Text(
          product.title!,
          style: const TextStyle(color: Colors.white),
        ),
        actions: [
          InkWell(
            splashFactory: NoSplash.splashFactory,
            highlightColor: ColorConstants.transparent,
            onTap: () {
              Navigator.pushNamed(context, '/wishlist');
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: SizedBox(
                height: 35,
                width: 35,
                child: Image.asset(
                  ImagePath.wishlist,
                  height: 20,
                  width: 20,
                  color: ColorConstants.black,
                ),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: bottomNavWidget(product: product),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              carousalSlider(),
              Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Container(
                      width: Dimensions.screenWidth,
                      height: 60,
                      color: Colors.black.withAlpha(50),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Container(
                      width: Dimensions.screenWidth,
                      height: 50,
                      color: Colors.black,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              product.title ?? "",
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontFamily: "Helvetica Neue",
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: Text(
                                'Rs. ' '${product.price ?? ""}',
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontFamily: "Helvetica Neue",
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget carousalSlider() {
    return CarouselSlider(
      items: [
        //1st Image of Slider
        Container(
          margin: const EdgeInsets.all(6.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: ColorConstants.appColor.withOpacity(0.7),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                alignment: Alignment.centerLeft,
                width: Dimensions.screenWidth,
                child: Center(
                  child: Image.network(
                    product.imageUrl!,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Positioned(
                bottom: 8,
                left: 0.0,
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        product.title!,
                        textScaleFactor: 1.0,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          color: ColorConstants.black,
                          fontSize: 18.0,
                          fontFamily: 'SofiaProMedium',
                          // fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        //
      ],

      //Slider Container properties
      options: CarouselOptions(
        height: ((1.16 * Dimensions.screenWidth) / 2),
        enlargeCenterPage: true,
        autoPlay: false,
        aspectRatio: 16 / 9,
        autoPlayCurve: Curves.fastOutSlowIn,
        enableInfiniteScroll: true,
        autoPlayAnimationDuration: const Duration(milliseconds: 2000),
        viewportFraction: 1,
      ),
    );
  }

  Widget bottomNavWidget({CategoryProduct? product}) {
    return BottomAppBar(
      child: Container(
          height: 70,
          // height: Dimensions.screenWidth / 5.0,
          width: Dimensions.screenWidth,
          padding: const EdgeInsets.all(
            15,
          ),
          decoration: const BoxDecoration(
              boxShadow: [
                BoxShadow(color: Colors.grey, spreadRadius: 0, blurRadius: 10),
              ],
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(0),
                topLeft: Radius.circular(0),
              ),
              color: Colors.white),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: const Icon(Icons.share),
                onPressed: () {},
              ),
              BlocBuilder<WishlistBloc, WishlistState>(
                builder: (context, state) {
                  return IconButton(
                    icon: const Icon(Icons.favorite),
                    onPressed: () {
                      context
                          .read<WishlistBloc>()
                          .add(AddWishlist(product: product));

                      const snackBar =
                          SnackBar(content: Text("Added to your Favourite"));
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    },
                  );
                },
              ),
              BlocBuilder<CartBloc, CartState>(
                builder: (context, state) {
                  if (state is CartLoadingState) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  if (state is CartLoadedState) {
                    return ElevatedButton(
                      child: const Text(
                        "ADD TO CART",
                        style: TextStyle(color: ColorConstants.black),
                      ),
                      onPressed: () {
                        context
                            .read<CartBloc>()
                            .add(AddToCart(product: product));
                        Navigator.pushNamed(context, "/cart");
                      },
                    );
                  }
                  if (state is CartErrorState) {
                    return buildError(state.message);
                  }
                  return Container();
                },
              ),
            ],
          )),
    );
  }

  Widget buildError(String message) {
    return Center(
      child: Text(message),
    );
  }
}
