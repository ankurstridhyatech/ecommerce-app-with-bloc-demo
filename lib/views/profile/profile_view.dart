import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/views/login/bloc/login_bloc.dart';
import 'package:ecommerce_bloc_demo/widgets/custom_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileView extends StatelessWidget {
  const ProfileView({Key? key}) : super(key: key);

  static const String routeName = '/profile';

  static Route route() {
    return MaterialPageRoute(
        settings: const RouteSettings(name: routeName),
        builder: (_) => const ProfileView());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: buildAppBar(),
        bottomNavigationBar: const CustomBottomBar(),
        body: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            return SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  const Info(
                    image: "assets/images/pic.png",
                    name: "Jhon Doe",
                    email: "Jhondoe01@gmail.com",
                  ),
                  SizedBox(height: Dimensions.screenWidth * 0.024 * 2), //20
                  ProfileMenuItem(
                    iconSrc: "assets/icons/User_Icon.svg",
                    title: "My Account",
                    press: () {},
                  ),
                  ProfileMenuItem(
                    iconSrc: "assets/icons/notification.svg",
                    title: "Notifications",
                    press: () {},
                  ),
                  ProfileMenuItem(
                    iconSrc: "assets/icons/help1.svg",
                    title: "Help",
                    press: () {},
                  ),
                  ProfileMenuItem(
                    iconSrc: "assets/icons/logout.svg",
                    title: "Logout",
                    press: () {
                      _signOut(context);
                    },
                  ),
                ],
              ),
            );
          },
        ));
  }

  void _signOut(context) {
    BlocProvider.of<AuthBloc>(context).add(
      SignOutRequested(),

    );
    Navigator.pushNamed(context, "/login");
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: ColorConstants.appColor,
      elevation: 0,
      leading: const SizedBox(),
      // On Android it's false by default
      centerTitle: true,
      title: const Text("Profile"),
    );
  }
}

class Info extends StatelessWidget {
  const Info({
    Key? key,
    this.name,
    this.email,
    this.image,
  }) : super(key: key);
  final String? name, email, image;

  @override
  Widget build(BuildContext context) {
    //   double defaultSize = SizeConfig.defaultSize;
    return SizedBox(
      height: Dimensions.screenWidth * 0.024 * 24, // 240
      child: Stack(
        children: <Widget>[
          ClipPath(
            clipper: CustomShape(),
            child: Container(
              height: Dimensions.screenWidth * 0.024 * 15, //150
              color: ColorConstants.appColor,
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      bottom: Dimensions.screenWidth * 0.024), //10
                  height: Dimensions.screenWidth * 0.024 * 14, //140
                  width: Dimensions.screenWidth * 0.024 * 14,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Colors.white,
                      width: Dimensions.screenWidth * 0.024 * 0.8, //8
                    ),
                    image: const DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage("assets/images/book_wallpaper.jpg"),
                    ),
                  ),
                ),
                Text(
                  name!,
                  style: TextStyle(
                    fontSize: Dimensions.screenWidth * 0.024 * 2.2, // 22
                    color: ColorConstants.black,
                  ),
                ),
                SizedBox(height: Dimensions.screenWidth * 0.024 / 2), //5
                Text(
                  email!,
                  style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF8492A2),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class CustomShape extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    double height = size.height;
    double width = size.width;
    path.lineTo(0, height - 100);
    path.quadraticBezierTo(width / 2, height, width, height - 100);
    path.lineTo(width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class ProfileMenuItem extends StatelessWidget {
  const ProfileMenuItem({
    Key? key,
    this.iconSrc,
    this.title,
    this.press,
  }) : super(key: key);
  final String? iconSrc, title;
  final VoidCallback? press;

  @override
  Widget build(BuildContext context) {
    double defaultSize = Dimensions.screenWidth * 0.024;
    return InkWell(
      onTap: press,
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: defaultSize * 2, vertical: defaultSize * 3),
        child: SafeArea(
          child: Row(
            children: <Widget>[
              SvgPicture.asset(
                iconSrc!,
                height: 20,
                width: 20,
                color: ColorConstants.black,
              ),
              SizedBox(width: defaultSize * 2),
              Text(
                title!,
                style: TextStyle(
                  fontSize: defaultSize * 1.6, //16
                  color: ColorConstants.black,
                ),
              ),
              const Spacer(),
              Icon(
                Icons.arrow_forward_ios,
                size: defaultSize * 1.6,
                color: ColorConstants.black,
              )
            ],
          ),
        ),
      ),
    );
  }
}
