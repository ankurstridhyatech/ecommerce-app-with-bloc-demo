part of 'signup_bloc.dart';

abstract class SignUpEvent extends Equatable {
  @override
  List<Object> get props => [];
}



// When the user signing up with email and password this event is called and the [AuthRepository] is called to sign up the user
class SignUpRequested extends SignUpEvent {
  final String email;
  final String password;

  SignUpRequested(this.email, this.password);
}

