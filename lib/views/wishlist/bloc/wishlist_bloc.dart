import 'package:ecommerce_bloc_demo/model/models.dart';
import 'package:ecommerce_bloc_demo/model/wishlist_model.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_event.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WishlistBloc extends Bloc<WishlistEvent, WishlistState> {
  WishlistBloc() : super(WishlistLoadingState()) {
    on<StartWishlist>((event, emit) async {
      await Future<void>.delayed(const Duration(seconds: 1));
      emit(WishlistLoadingState());
      try {
        emit(WishlistLoadedState());
      } catch (e) {
        emit(WishlistErrorState(
          message: e.toString(),
        ));
      }
    });

    on<AddWishlist>((AddWishlist event, emit) async {
      final state = this.state; // local variable

      // emit(WishlistLoadingState());

      if (state is WishlistLoadedState) {
        try {
          emit(WishlistLoadedState(
              wishlist: Wishlist(
                  products: List.from((state).wishlist!.products!)
                    ..add(event.product!))));

        } catch (e) {
          emit(WishlistErrorState(
            message: e.toString(),
          ));
        }
      }
    });

    on<RemoveWishlist>((RemoveWishlist event, emit) async {
      final state = this.state; // local variable
      // emit(WishlistLoadingState());

      if (state is WishlistLoadedState) {
        try {
          emit(WishlistLoadedState(
              wishlist: Wishlist(
                  products: List.from(state.wishlist!.products!)
                    ..remove(event.product!))));


        } catch (e) {
          emit(WishlistErrorState(
            message: e.toString(),
          ));
        }
      }
    });
  }

  Stream<WishlistState> mapEventToState(WishlistEvent event) async* {
    if (event is StartWishlist) {
      yield* _mapStartWishlistToState();
    } else if (event is AddWishlist) {
      yield* _mapAddWishlistToState(event, state);
    } else if (event is RemoveWishlist) {
      yield* _mapRemoveWishlistToState(event, state);
    }
  }

  Stream<WishlistState> _mapStartWishlistToState() async* {
    yield WishlistLoadingState();
    try {
      await Future<void>.delayed(const Duration(seconds: 1));
      yield WishlistLoadedState();
    } catch (_) {}
  }

  Stream<WishlistState> _mapAddWishlistToState(
    AddWishlist event,
    WishlistState state,
  ) async* {
    if (state is WishlistLoadedState) {
      try {
        yield WishlistLoadedState(
            wishlist: Wishlist(
                products: List.from(state.wishlist!.products!)
                  ..add(event.product!)));
      } catch (_) {}
    }
  }

  Stream<WishlistState> _mapRemoveWishlistToState(
    RemoveWishlist event,
    WishlistState state,
  ) async* {
    if (state is WishlistLoadedState) {
      try {
        yield WishlistLoadedState(
            wishlist: Wishlist(
                products: List.from(state.wishlist!.products!)
                  ..remove(event.product!)));
      } catch (_) {}
    }
  }
}

