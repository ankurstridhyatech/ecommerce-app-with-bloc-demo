import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:equatable/equatable.dart';
import 'package:ecommerce_bloc_demo/model/models.dart';

abstract class WishlistEvent extends Equatable {
  const WishlistEvent();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class StartWishlist extends WishlistEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class AddWishlist extends WishlistEvent {
  final CategoryProduct? product;

  const AddWishlist({this.product});

  @override
  // TODO: implement props
  List<Object> get props => [product!];
}

class RemoveWishlist extends WishlistEvent {
  final CategoryProduct? product;

  const RemoveWishlist({this.product});

  @override
  // TODO: implement props
  List<Object> get props => [product!];
}
