// import 'package:ecommerce_bloc_demo/model/wishlist_model.dart';
import 'package:ecommerce_bloc_demo/model/models.dart';
import 'package:equatable/equatable.dart';

abstract class WishlistState extends Equatable {
  const WishlistState();

  @override
  List<Object> get props => [];
}

class WishlistInitialState extends WishlistState {
  @override
  List<Object> get props => [];
}

class WishlistLoadingState extends WishlistState {
  @override
  List<Object> get props => [];
}

class WishlistLoadedState extends WishlistState {
  final Wishlist? wishlist;

  WishlistLoadedState({this.wishlist = const Wishlist()});

  @override
  List<Object> get props => [wishlist!];
}

class WishlistErrorState extends WishlistState {
  String message;

  WishlistErrorState({required this.message});

  @override
  List<Object> get props => [message];
}
