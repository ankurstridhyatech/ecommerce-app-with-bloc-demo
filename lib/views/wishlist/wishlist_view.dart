// import 'package:ecommerce_bloc_demo/controller/order_controller.dart';
import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_bloc.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_state.dart';
import 'package:ecommerce_bloc_demo/widgets/custom_bottom_bar.dart';
import 'package:ecommerce_bloc_demo/widgets/wishlist_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WishlistView extends StatelessWidget {
  WishlistView({Key? key}) : super(key: key);

  static const String routeName = '/wishlist';

  static Route route() {
    return MaterialPageRoute(
        settings: RouteSettings(name: routeName),
        builder: (_) => WishlistView());
  }

  @override
  Widget build(BuildContext context) {
    // var cartController = Get.put(CartController());
    // var orderController = Get.put(OrderController());

    return Scaffold(
        backgroundColor: ColorConstants.white,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: ColorConstants.appColor,
          title: Text(
            "Wishlist",
            style: TextStyle(color: Colors.white),
          ),
        ),
        // bottomNavigationBar: CustomBottomBar(),
        body:
            BlocBuilder<WishlistBloc, WishlistState>(builder: (context, state) {
          if (state is WishlistLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is WishlistLoadedState) {
            return SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
                child: state.wishlist != null && state.wishlist!.products != null ? GridView.builder(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 1, childAspectRatio: 2.4),
                  itemCount: state.wishlist!.products!.length,
                  itemBuilder: (context, index) => WishlistCard(
                     product: state.wishlist!.products![index],
                    isFavourite: true,
                  ),
                ) : Container(),
              ),
            );
          }
          if (state is WishlistErrorState) {
            return buildError(state.message);
          }
          return Container();
        }));
  }

  // List<CategoryProduct> categoryProducts = [
  //   CategoryProduct(
  //     id: 1,
  //     title: 'Calculator',
  //     price: 20,
  //     imageUrl:
  //         'https://tiimg.tistatic.com/fp/1/006/392/eco-friendly-school-note-book-243-w410.jpg',
  //     // 'assets/images/calculator.png',
  //   ),
  //   CategoryProduct(
  //       id: 2,
  //       title: 'Office Stationery',
  //       price: 20,
  //
  //       // imageUrl: 'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Men-Legend-BlackMatte-3.4_672x672.jpg?v=1600886623'),
  //       imageUrl:
  //           'https://images-na.ssl-images-amazon.com/images/I/81yYeKc+U7L.jpg'
  //       // 'assets/images/office_supplies.png'
  //       ),
  //   CategoryProduct(
  //       id: 3,
  //       title: 'Notebook',
  //       price: 20,
  //       imageUrl: 'https://thumbs.dreamstime.com/z/stationery-set-22104838.jpg'
  //       // 'assets/images/notebook.png'
  //       ),
  //   CategoryProduct(
  //       id: 4,
  //       title: 'Calender',
  //       price: 20,
  //       imageUrl: 'https://thumbs.dreamstime.com/z/calender-4737855.jpg'
  //       // 'assets/images/calendar.png'
  //       ),
  // ];

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildError(String message) {
    return Center(
      child: Text(message),
    );
  }
}
