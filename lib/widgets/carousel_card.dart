import 'package:ecommerce_bloc_demo/model/models.dart';
import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:flutter/material.dart';

class CarouselCard extends StatelessWidget {
  const CarouselCard({Key? key, required this.category}) : super(key: key);

  final Category category;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(6.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              color: ColorConstants.appColor.withOpacity(0.7),
              borderRadius: BorderRadius.circular(8.0),
            ),
            alignment: Alignment.centerLeft,
            width: Dimensions.screenWidth,
            child: Image.network(
                // "https://www.pngkey.com/png/full/443-4432695_school-library-online-book-store.png",
                category.imageUrl!),
          ),
          Positioned(
            // top: (((1.16 * Dimensions.screenWidth) / 2) - 50),
            top: 0,
            left: 0.0,
            right: 0.0,
            child: Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    // "School Book Store",
                    category.title!,
                    textScaleFactor: 1.0,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      color: ColorConstants.black,
                      fontSize: 18.0,
                      fontFamily: 'SofiaProMedium',
                      // fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            // top: (((1.16 * Dimensions.screenWidth) / 2) - 50),
            top: Dimensions.screenWidth * 0.20,
            left: 0.0,
            right: 0.0,
            child: InkWell(
              onTap: () {
                // Get.toNamed(Routes.SCHOOLBOOKS);
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: ColorConstants.grayf4,
                          borderRadius: BorderRadius.circular(20)),
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "Buy Now",
                          textScaleFactor: 1.0,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: ColorConstants.black,
                            fontSize: 14.0,
                            fontFamily: 'SofiaProMedium',
                            // fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
