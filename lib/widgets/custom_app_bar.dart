import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/utils/image_paths.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  Widget build(
    BuildContext context,
  ) {
    return SafeArea(
        child: Container(
      color: ColorConstants.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding:
                const EdgeInsets.only(right: 25, left: 10, top: 5, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  icon: const Icon(
                    Icons.menu,
                    color: CupertinoColors.black,
                  ),
                  color: ColorConstants.brown,
                  iconSize: Dimensions.screenHeight / 30,
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 0.0),
                  child: Container(
                    alignment: Alignment.center,
                    width: Dimensions.screenWidth / 2.0,
                    height: 40,
                    child: Text(
                      title!,
                      style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: ColorConstants.black),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                InkWell(
                  splashFactory: NoSplash.splashFactory,
                  highlightColor: ColorConstants.transparent,
                  onTap: () {
                    Navigator.pushNamed(context, '/wishlist');
                  },
                  child: SizedBox(
                    height: 35,
                    width: 35,
                    child: Image.asset(
                      ImagePath.wishlist,
                      height: 20,
                      width: 20,
                      color: ColorConstants.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 1,
            color: ColorConstants.grey,
          )
        ],
      ),
    ));
  }

  @override
  Size get preferredSize => const Size.fromHeight(65);
}
