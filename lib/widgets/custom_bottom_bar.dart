import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/utils/image_paths.dart';
import 'package:flutter/material.dart';

class CustomBottomBar extends StatelessWidget {
  const CustomBottomBar({Key? key}) : super(key: key);

  @override
  Widget build(
    BuildContext context,
  ) {
    return SafeArea(
        child: BottomAppBar(
      child: Container(
          height: 70,
          width: Dimensions.screenWidth,
          padding: const EdgeInsets.all(
            15,
          ),
          decoration: const BoxDecoration(
              boxShadow: [
                BoxShadow(color: Colors.grey, spreadRadius: 0, blurRadius: 10),
              ],
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(0),
                topLeft: Radius.circular(0),
              ),
              color: Colors.white),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              item(0, "Home", ImagePath.home, () {
                Navigator.pushNamed(context, '/home');
              }),
              item(2, "Cart", ImagePath.cart, () {
                Navigator.pushNamed(context, '/cart');
              }),
              item(3, "Profile", ImagePath.profile, () {
                Navigator.pushNamed(context, '/profile');
              })
            ],
          )),
    ));
  }

  item(
    index,
    name,
    imgName,
    VoidCallback? onPress,
  ) {
    return Material(
      color: Colors.white.withOpacity(.5),
      borderRadius: const BorderRadius.all(Radius.circular(9)),
      child: InkWell(
        splashColor: Colors.white.withOpacity(.1),
        borderRadius: const BorderRadius.all(Radius.circular(500)),
        enableFeedback: true,
        onTap: onPress,
        child: Container(
          margin: const EdgeInsets.all(8),
          child: Image(
              height: Dimensions.screenWidth / 18,
              width: Dimensions.screenWidth / 18,
              color: ColorConstants.appColor,
              image: AssetImage(imgName)),
        ),
      ),
    );
  }

  Size get preferredSize => const Size.fromHeight(65);
}
