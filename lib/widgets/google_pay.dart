import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:flutter/material.dart';
import 'package:pay/pay.dart';

class GooglePay extends StatelessWidget {
  const GooglePay({Key? key, this.total, this.products}) : super(key: key);

  final String? total;
  final List<CategoryProduct>? products;

  @override
  Widget build(BuildContext context) {
    var paymentItems = products!
        .map((product) => PaymentItem(
            label: product.title,
            amount: product.price.toString(),
            status: PaymentItemStatus.final_price,
            type: PaymentItemType.item))
        .toList();

    paymentItems.add(PaymentItem(
      label: 'Total',
      amount: total!,
      type: PaymentItemType.total,
      status: PaymentItemStatus.final_price,
    ));

    void onGooglePaymentResult(paymentResult) {
      debugPrint(paymentResult.toString());
    }

    return GooglePayButton(
      paymentConfigurationAsset: "payment_profile_google_pay.json",
      onPaymentResult: onGooglePaymentResult,
      paymentItems: paymentItems,
      type: GooglePayButtonType.pay,
      margin: const EdgeInsets.only(top: 15.0),
      loadingIndicator: const Center(
        child: CircularProgressIndicator(),
      ),
      // onError: ,
    );
  }
}
