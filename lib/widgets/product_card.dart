import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/views/cart/bloc/cart_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductCard extends StatelessWidget {
  const ProductCard({Key? key, this.product}) : super(key: key);

  final CategoryProduct? product;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/category', arguments: product);
      },
      child: Container(
          height: Dimensions.screenHeight * 0.14,
          width: Dimensions.screenHeight * 0.18,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border:
                  Border.all(color: ColorConstants.gray70.withOpacity(0.2))),
          child: ClipRRect(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(8),
              topRight: Radius.circular(8),
              bottomLeft: Radius.circular(8),
              bottomRight: Radius.circular(8),
            ),
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                          product!.imageUrl.toString(),
                        ))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      height: Dimensions.screenHeight * 0.09,
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8, vertical: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: ColorConstants.white,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: Dimensions.screenWidth * 0.2,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  product!.title.toString(),
                                  maxLines: 2,
                                  style: const TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          BlocBuilder<CartBloc, CartState>(
                            builder: (context, state) {
                              if (state is CartLoadingState) {
                                return const Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                              if (state is CartLoadedState) {
                                return SizedBox(
                                  height: 28,
                                  width: 28,
                                  child: MaterialButton(
                                    onPressed: () {
                                      context
                                          .read<CartBloc>()
                                          .add(AddToCart(product: product));
                                      const snackBar = SnackBar(
                                          content: Text("Added to your Cart"));
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar);
                                    },
                                    color: ColorConstants.appColor,
                                    textColor: Colors.white,
                                    padding: const EdgeInsets.all(2),
                                    shape: const CircleBorder(),
                                    child: const Icon(
                                      Icons.add,
                                      // size: 24,
                                    ),
                                  ),
                                );
                              }
                              if (state is CartErrorState) {
                                return buildError(state.message);
                              }
                              return Container();
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
          )),
    );
  }

  Widget buildError(String message) {
    return Center(
      child: Text(message),
    );
  }
}
