import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:flutter/material.dart';

class RoundedBorderButton extends StatelessWidget {
  final String buttonName;
  final Color color;
  final Color borderColor;
  final Color textColor;
  final VoidCallback? onTap;

  const RoundedBorderButton(
      {Key? key,
      required this.color,
      required this.borderColor,
      required this.onTap,
      required this.buttonName,
      required this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTap,
        child: Container(
          width: Dimensions.screenWidth,
          height: Dimensions.screenWidth * 0.12,
          decoration: BoxDecoration(
              color: color,
              border: Border.all(color: borderColor),
              borderRadius: const BorderRadius.all(Radius.circular(30))),
          child: Center(
              child: Text(
            buttonName,
            style: TextStyle(fontSize: 16, color: textColor),
          )),
        ));
  }
}
