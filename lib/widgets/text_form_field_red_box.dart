import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFormFieldWidgetRedBox extends StatelessWidget {
  TextFormFieldWidgetRedBox(
      {Key? key,
      this.controller,
      this.labelText,
      this.validator,
      this.hintText,
      this.callback,
      this.length,
      this.isObscureText,
      this.passwordButton,
      required this.inputType,
      this.inputAction,
      this.enabled,
      this.inputFormatters,
      this.autovalidateMode,
      this.onChanged,
      this.onSaved,
      this.isError,
      this.isBox})
      : super(key: key);

  final TextEditingController? controller;
  final String? labelText;
  final int? length;
  final String? Function(String?)? validator;
  final VoidCallback? callback;
  final bool? isObscureText;
  final String? hintText;
  final Widget? passwordButton;
  final TextInputType inputType;
  final TextInputAction? inputAction;
  final AutovalidateMode? autovalidateMode;
  final List<TextInputFormatter>? inputFormatters;
  bool? enabled = true;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String>? onSaved;
  bool? isError = false;
  bool? isBox = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Container(
        height: 48, //Dimensions.screenHeight / 13.3,
        //height: 54,
        decoration: BoxDecoration(
            border: Border.all(
              color: isError == true
                  ? Colors.red
                  : isBox == true
                      ? Colors.red
                      : ColorConstants.grayE5,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(8))),
        child: Padding(
          padding: const EdgeInsets.only(left: 0.0),
          child: Theme(
            data: ThemeData.dark(),
            //data:theme.copyWith(primaryColor: Colors.red),
            child: TextFormField(
              cursorColor: ColorConstants.appColor,
              textInputAction: inputAction,
              controller: controller,
              maxLength: length,
              validator: validator,
              // obscureText: isObscureText!,
              enabled: enabled,
              keyboardType: inputType,
              autofocus: false,
              textAlign: TextAlign.start,
              onChanged: onChanged,
              style: const TextStyle(fontSize: 16, color: ColorConstants.black),
              decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(
                      top: 7.0, bottom: 7.0, left: 14, right: 10),
                  filled: true,
                  hintText: hintText,
                  fillColor: Colors.transparent,
                  suffixIcon: passwordButton,
                  labelText: labelText,
                  labelStyle: UiUtils().text_style(
                      16,
                      isError == true ? Colors.red : ColorConstants.gray7A,
                      // AppConstants.fontRegular,
                      FontWeight.w400),
                  border: InputBorder.none),
            ),
          ),
        ),
      ),
    );
  }
}
