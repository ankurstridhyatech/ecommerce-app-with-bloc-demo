import 'package:ecommerce_bloc_demo/model/product.dart';
import 'package:ecommerce_bloc_demo/style/color_constants.dart';
import 'package:ecommerce_bloc_demo/style/dimensions.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_bloc.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_event.dart';
import 'package:ecommerce_bloc_demo/views/wishlist/bloc/wishlist_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WishlistCard extends StatelessWidget {
  const WishlistCard({
    Key? key,
    this.width,
    this.product,
    this.isFavourite,
  }) : super(key: key);

  final double? width;
  final CategoryProduct? product;
  final bool? isFavourite;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 8),
      child: Stack(
        children: [
          Container(
            width: Dimensions.screenWidth,
            height: 150,
            color: ColorConstants.appColor.withOpacity(0.2),
            child: Image.network(product!.imageUrl!),
          ),
          Positioned(
              top: 60,
              left: 100,
              child: Container(
                width: Dimensions.screenWidth - 160,
                height: 60,
                color: Colors.black.withAlpha(50),
              )),
          Positioned(
              top: 65,
              left: 105,
              child: Container(
                width: Dimensions.screenWidth - 170,
                height: 50,
                color: Colors.black,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            product?.title ?? "",
                            style: const TextStyle(
                              color: Colors.white,
                              fontFamily: "Helvetica Neue",
                            ),
                          ),
                          Text(
                            'Rs. ' '${product?.price ?? ""}',
                            style: const TextStyle(
                              color: Colors.white,
                              fontFamily: "Helvetica Neue",
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Row(
                          children: [
                            BlocBuilder<WishlistBloc, WishlistState>(
                                builder: (context, state) {
                              return SizedBox(
                                height: 24,
                                width: 24,
                                child: MaterialButton(
                                  onPressed: () {
                                    context
                                        .read<WishlistBloc>()
                                        .add(AddWishlist(product: product));
                                  },
                                  color: ColorConstants.appColor,
                                  textColor: Colors.white,
                                  padding: const EdgeInsets.all(0),
                                  shape: const CircleBorder(),
                                  child: const Icon(
                                    Icons.add,
                                    // size: 24,
                                  ),
                                ),
                              );
                            }),
                            const SizedBox(
                              width: 10,
                            ),
                            BlocBuilder<WishlistBloc, WishlistState>(
                                builder: (context, state) {
                              return SizedBox(
                                height: 24,
                                width: 24,
                                child: MaterialButton(
                                  onPressed: () {
                                    context
                                        .read<WishlistBloc>()
                                        .add(RemoveWishlist(product: product));
                                  },
                                  color: ColorConstants.appColor,
                                  textColor: Colors.white,
                                  padding: const EdgeInsets.all(0),
                                  shape: const CircleBorder(),
                                  child: const Icon(
                                    Icons.delete,
                                    size: 15,
                                  ),
                                ),
                              );
                            }),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
